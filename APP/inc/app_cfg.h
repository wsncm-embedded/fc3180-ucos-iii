/*
 *********************************************************************************************************
 *                                             APPLICATION
 *                                            EXAMPLE CODE
 *
 *                          (c) Copyright 2009-2014; Micrium, Inc.; Weston, FL
 *
 *               All rights reserved. Protected by international copyright laws.
 *
 *               Please feel free to use any application code labeled as 'EXAMPLE CODE' in
 *               your application products. Example code may be used as is, in whole or in
 *               part, or may be used as a reference only.
 *
 *               Please help us continue to provide the Embedded community with the finest
 *               software available. Your honesty is greatly appreciated.
 *
 *               You can contact us at www.micrium.com.
 *********************************************************************************************************
 */

/*
 *********************************************************************************************************
 *
 *                                       APPLICATION CONFIGURATION
 *
 *                                       Texas Instruments MSP430
 *                                               on the
 *                                           MSP-EXP430F5438
 *                                          Evaluation Board
 *
 * File          : app_cfg.h
 * Version       : V1.00
 * Programmer(s) : HS
 *
 *********************************************************************************************************
 */

#ifndef  APP_CFG_MODULE_PRESENT
#define  APP_CFG_MODULE_PRESENT

/*
 *********************************************************************************************************
 *                                       Task Delay definitions, Jack, 2015/11/30
 *********************************************************************************************************
 */

#define DELAY_TIMER 1000

#define RUN_TIMERS 3

#define RUN_COUNTER(X)        if(X++ >= RUN_TIMERS)\
								{\
									OSSemPend(&sem_pend, 0, OS_OPT_PEND_BLOCKING, NULL, NULL);\
								}

/*
 *********************************************************************************************************
 *                                       ADDITIONAL uC/MODULE ENABLES
 *********************************************************************************************************
 */


/*
 *********************************************************************************************************
 *                                           TASK PRIORITIES
 *********************************************************************************************************
 */

#define  APP_TASK_START_PRIO                   5u
#define  APP_TASK_1_PRIO                       6u
#define  APP_TASK_2_PRIO                       7u
#define  APP_TASK_3_PRIO                       8u

#define  APP_CFG_TASK_START_PRIO                              2u    //To match APP cases, Jack, 20151129
#define  APP_CFG_TASK_COMMON_PRIO                             3u

/*
 *********************************************************************************************************
 *                                          TASK STACK SIZES
 *********************************************************************************************************
 */

#define  APP_TASK_START_STK_SIZE             2048u
#define  APP_TASK_1_STK_SIZE                 2048u
#define  APP_TASK_2_STK_SIZE                 2048u
#define  APP_TASK_3_STK_SIZE                 2048u

#define  APP_CFG_TASK_COMMON_STK_SIZE                     1024//128u	  //To match APP cases, Jack, 20151129
#define  APP_CFG_TASK_START_STK_SIZE                      1024//128u



/*
 *********************************************************************************************************
 *                                           uC/LIB Module
 *********************************************************************************************************
 */

#define  LIB_MEM_CFG_ALLOC_EN                   DEF_ENABLED



/*
 *********************************************************************************************************
 *                                              TRACING
 *********************************************************************************************************
 */

#include  <stdio.h>
#define  TRACE_LEVEL_OFF                                0u
#define  TRACE_LEVEL_INFO                               1u
#define  TRACE_LEVEL_DBG                                2u

#define  APP_CFG_TRACE_LEVEL                     TRACE_LEVEL_OFF
#define  APP_CFG_TRACE                              printf/*BSP_Ser_Printf*/

#define  APP_TRACE_INFO(x)    ((APP_CFG_TRACE_LEVEL >= TRACE_LEVEL_INFO) ? (void)(APP_CFG_TRACE x) : (void)0)
#define  APP_TRACE_DBG(x)     ((APP_CFG_TRACE_LEVEL >= TRACE_LEVEL_DBG)  ? (void)(APP_CFG_TRACE x) : (void)0)

#endif
