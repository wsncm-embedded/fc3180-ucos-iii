/*
*********************************************************************************************************
*                                              EXAMPLE CODE
*
*                          (c) Copyright 2003-2014; Micrium, Inc.; Weston, FL
*
*               All rights reserved.  Protected by international copyright laws.
*               Knowledge of the source code may NOT be used to develop a similar product.
*               Please help us continue to provide the Embedded community with the finest
*               software available.  Your honesty is greatly appreciated.
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                            EXAMPLE CODE
*

* Filename      : app.c
* Version       : V1.00
* Programmer(s) : 
*********************************************************************************************************
*/

/*Mark for git.*/

#include <includes.h>

/*
*********************************************************************************************************
*                                                 DEFINES
*********************************************************************************************************
*/
#define OutTMR_ADDR	OutCM					//timer config, Jack
#define InTMR_ADDR	InCM
#define AndTMR_ADDR	AndCM
#define OrTMR_DDR	OrCM
#define XorTMR_ADDR	XorCM

#define OutTMR_DATA(d)	asm("out.port",(d),(dm_index))
#define InWTMR_ATA() 	(unsigned char)asm("in.port",(dm_index))
#define AndTMR_DATA(d)	asm("and.port",(d),(dm_index))
#define OrTMR_DATA(d) 	asm("or.port",(d),(dm_index))
#define XorTMR_DATA(d)	asm("xor.port",(d),(dm_index))

#define MCODE_EIR_SET(VECTOR)             asm("setbit eir", (VECTOR))

/*
*********************************************************************************************************
*                                                VARIABLES
*********************************************************************************************************
*/

static  OS_TCB      AppTaskStartTCB;
static  OS_TCB      AppTask1TCB;
static  OS_TCB      AppTask2TCB;
static  OS_TCB      AppTask3TCB;

static  CPU_STK     AppTaskStartStk[APP_TASK_START_STK_SIZE];
static  CPU_STK     AppTask1Stk[APP_TASK_1_STK_SIZE];
static  CPU_STK     AppTask2Stk[APP_TASK_2_STK_SIZE];
static  CPU_STK     AppTask3Stk[APP_TASK_3_STK_SIZE];

static unsigned char timer_cnt = 0;		//timer interrupt coun, Jack
static unsigned char adc_cnt = 0;

unsigned char ADC_Val[2]={0};


/*
*********************************************************************************************************
*                                            FUNCTION PROTOTYPES
*********************************************************************************************************
*/

void  AppTaskCreate (void);
void  AppTaskStart  (void       *p_arg);
void  AppTask1      (void       *p_arg);
void  AppTask2      (void       *p_arg);
void  AppTask3      (void       *p_arg);

void timer_init();
void ADC_init();
static void timer_isr();
static void ADC_isr();

/*$PAGE*/
/*
*********************************************************************************************************
*                                                main()
*
* Description : This is the standard entry point for C code.  It is assumed that your code will call
*               main() once you have performed all necessary CPU and C initialization.
*
* Arguments   : none
*********************************************************************************************************
*/

void  main (int argc, char *argv[])
{
    OS_ERR  err;


//    BSP_IntDisAll();                                            /* Disable all int. until we are ready to accept them   */

    OSInit(&err);                                               /* Init uC/OS-III.                                      */

    OSTaskCreate((OS_TCB     *)&AppTaskStartTCB,                /* Create the start task                                */
                 (CPU_CHAR   *)"App Task Start",
                 (OS_TASK_PTR ) AppTaskStart,
                 (void       *) 0,
                 (OS_PRIO     ) APP_TASK_START_PRIO,
                 (CPU_STK    *)&AppTaskStartStk[0],
                 (CPU_STK_SIZE) APP_TASK_START_STK_SIZE / 10u,
                 (CPU_STK_SIZE) APP_TASK_START_STK_SIZE,
                 (OS_MSG_QTY  ) 0u,
                 (OS_TICK     ) 0u,
                 (void       *) 0,
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR     *)&err);

    OSStart(&err);                                              /* Start multitasking (i.e. give control to uC/OS-III). */
}


/*$PAGE*/
/*
*********************************************************************************************************
*                                               AppTaskStart()
*
* Description : This is an example of a startup task.  As mentioned in the book's text, you MUST
*               initialize the ticker only once multitasking has started.
*
* Arguments   : p_arg   is the argument passed to 'AppStartTask()' by 'OSTaskCreate()'.
*
* Note(s)     : 1) The first line of code is used to prevent a compiler warning because 'p_arg' is not
*                  used.  The compiler should not generate any code for this statement.
*               2) Interrupts are enabled once the task start because the I-bit of the CCR register was
*                  set to 0 by 'OSTaskCreate()'.
*********************************************************************************************************
*/

static  void  AppTaskStart (void *p_arg)
{
    OS_ERR  err;


    (void)p_arg;

    BSP_Init();                                                 /* Initialize BSP functions                             */

#if (OS_CFG_STAT_TASK_EN > 0u)
    OSStatTaskCPUUsageInit(&err);                               /* Compute CPU capacity with no task running            */
#endif

    Mem_Init();                                                 /* Initialize memory management module                  */

    AppTaskCreate(); 
                                            /* Create application tasks                             */

    while (DEF_TRUE) {                                          /* Task body, always written as an infinite loop.       */

        OSTimeDlyHMSM(0u, 0u, 0u, 100u,
                      OS_OPT_TIME_HMSM_STRICT,
                      &err);
    }
}


/*$PAGE*/
/*
*********************************************************************************************************
*                                               AppTaskCreate()
*
* Description: This function creates the application tasks.
*
* Arguments  : none.
*
* Note(s)    : none.
*********************************************************************************************************
*/

static  void  AppTaskCreate (void)
{
    OS_ERR  err;


    OSTaskCreate((OS_TCB     *)&AppTask1TCB,                    /* Create Task 1                                        */
                 (CPU_CHAR   *)"App Task 1",
                 (OS_TASK_PTR ) AppTask1,
                 (void       *) 0,
                 (OS_PRIO     ) APP_TASK_1_PRIO,
                 (CPU_STK    *)&AppTask1Stk[0],
                 (CPU_STK_SIZE) APP_TASK_1_STK_SIZE / 10u,
                 (CPU_STK_SIZE) APP_TASK_1_STK_SIZE,
                 (OS_MSG_QTY  ) 0u,
                 (OS_TICK     ) 0u,
                 (void       *) 0,
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR     *)&err);

    OSTaskCreate((OS_TCB     *)&AppTask2TCB,                    /* Create Task 2                                        */
                 (CPU_CHAR   *)"App Task 2",
                 (OS_TASK_PTR ) AppTask2,
                 (void       *) 0,
                 (OS_PRIO     ) APP_TASK_2_PRIO,
                 (CPU_STK    *)&AppTask2Stk[0],
                 (CPU_STK_SIZE) APP_TASK_2_STK_SIZE / 10u,
                 (CPU_STK_SIZE) APP_TASK_2_STK_SIZE,
                 (OS_MSG_QTY  ) 0u,
                 (OS_TICK     ) 0u,
                 (void       *) 0,
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR     *)&err);

    OSTaskCreate((OS_TCB     *)&AppTask3TCB,                    /* Create Task 3                                        */
                 (CPU_CHAR   *)"App Task 3",
                 (OS_TASK_PTR ) AppTask3,
                 (void       *) 0,
                 (OS_PRIO     ) APP_TASK_3_PRIO,
                 (CPU_STK    *)&AppTask3Stk[0],
                 (CPU_STK_SIZE) APP_TASK_3_STK_SIZE / 10u,
                 (CPU_STK_SIZE) APP_TASK_3_STK_SIZE,
                 (OS_MSG_QTY  ) 0u,
                 (OS_TICK     ) 0u,
                 (void       *) 0,
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR     *)&err);
}


/*
*********************************************************************************************************
*                                                AppTask1()
*
* Description : 
*
* Arguments   : p_arg   is the argument passed to 'AppTask2()' by 'OSTaskCreate()'.
*
* Returns     : none
*********************************************************************************************************
*/

void  AppTask1 (void *p_arg)
{
    OS_ERR  err;
	int a=0;


/**********************************************************
 Testing for installing interrupts, Jack, 20151105
***********************************************************/
    //timer_init();                            /* Initiate the timer    */
	//ADC_init();                                       
  	sysSetEventVector(EVENTLEVEL_ADC,ADC_isr);
  	MCODE_EIR_SET(EVENTLEVEL_ADC);		 //ISR won't run without this sentence. Jack
  	//asm("enb");
  	ADC_init(); 

/***********************************************************/

	OrCB(0x08);     //'00001000',再令PB3方向为out
    while (DEF_TRUE) {
 	    //OrIRQ0_EN(0x04);
	    a++;

	    /*OSTimeDlyHMSM() will go to ERROR:OS_ERR_TIME_DLY_ISR, because OSIntNestingCtr >0,
		and make it looping here, jack*/

	    //APP_CFG_TRACE("test\n");
	    //printf("AppTask1, a=%d\n", a);

		AndDB(0xF7);    	// 数据与'11110111',令PC6为低，igt初始为高电平
		OSTimeDlyHMSM(0u, 0u, 0u, 500u,
                      OS_OPT_TIME_HMSM_STRICT,
                      &err);

		OrDB(0x08); 		// '00001000'
		OSTimeDlyHMSM(0u, 0u, 0u, 500u,
                      OS_OPT_TIME_HMSM_STRICT,
                      &err);
    }
}

/*
*********************************************************************************************************
*                                                AppTask2()
*
* Description : 
*
* Arguments   : p_arg   is the argument passed to 'AppTask3()' by 'OSTaskCreate()'.
*
* Returns     : none
*********************************************************************************************************
*/

void  AppTask2 (void *p_arg)
{
		OS_ERR	err;
		int a=0;
		int com1;
		unsigned char Frame[1] = {50};
	
	
	/**********************************************************
	 Testing for installing interrupts, Jack, 20151105
	***********************************************************/
			//timer_init(); 						   /* Initiate the timer	*/
			//ADC_init();										
		//sysSetEventVector(EVENTLEVEL_ADC,ADC_isr);
		//MCODE_EIR_SET(EVENTLEVEL_ADC);		 //ISR won't run without this sentence. Jack
			//asm("enb");
	
			//sysEventInstall(EVENTLEVEL_ADC,ADC_isr);
		//ADC_init(); 
	
			//halEventInit();
		comInit();
		//test(5);
	
	
		com1 = comOpen(3, 115200, DATABITS_8, STOPBITS_1, PARITY_NONE, FLOWCONTROL_RTSCTS, COM_READWRITE, 512);
		
		a = com1;
	/***********************************************************/
	
		while (DEF_TRUE) {
			//OrIRQ0_EN(0x04);
			OS_CRITICAL_ENTER();
			//while(1){
				a++;
			//comWaitData(com1);
		    if(comDataAvailable(com1))	   //Please use comDataAvailable() instead of comWaitData()
		    {
			    comRead(com1,Frame,1);
				comWrite(com1,Frame,1);
			}    
			//} 					   
			OS_CRITICAL_EXIT();
	
			/*OSTimeDlyHMSM() will go to ERROR:OS_ERR_TIME_DLY_ISR, because OSIntNestingCtr >0,
			and make it looping here, jack*/
	
			//APP_CFG_TRACE("test\n");
			//printf("AppTask1, a=%d\n", a);
	
			OSTimeDlyHMSM(0u, 0u, 0u, 5000u,
						  OS_OPT_TIME_HMSM_STRICT,
						  &err);
		}

}


/*
*********************************************************************************************************
*                                                AppTask3()
*
* Description : 
*
* Arguments   : p_arg   is the argument passed to 'AppTask3()' by 'OSTaskCreate()'.
*
* Returns     : none
*********************************************************************************************************
*/

void  AppTask3 (void *p_arg)
{
    OS_ERR  err;

	int c=0;

    while (DEF_TRUE) {
		c++;
	    //MCODE_EIR_SET(EVENTLEVEL_ADC);
	    //printf("AppTask3, a=%d\n", a);
		OSTimeDlyHMSM(0u, 0u, 0u, 100u,
                      OS_OPT_TIME_HMSM_STRICT,
                      &err);
    }
}

void timer_init()
{
	OrCRB(CRB3_index, 0xc0);	//'1100 0000' enable timer and start running
	OrIRQ0_EN(0x02);	//enable timer interrupt

	AndTMR_DATA(0x00);	// clear IFL register
	OutTMR_ADDR(0xB8);

	OutTMR_DATA(0xA0);	//"1010 0000" REP:1, EVT:0, DRV:1, driving the adjacent channel
	OutTMR_ADDR(0xA0);	//ch0
	OutTMR_ADDR(0xA1);	//ch1
	OutTMR_ADDR(0xA2);	//ch2
	OutTMR_DATA(0x80);	//"1000 0000" REP:1, 
	OutTMR_ADDR(0xA3);	//ch3

	OutTMR_DATA(0xFD);	//"1111 1110" EXP=7, MSA=29, counter clock 83.3MHz/(2*EXP)~1.53us
	OutTMR_ADDR(0x80);	//ch0
	OutTMR_DATA(0x15);	//"0001 0101" EXP=0, MSA=21
	OutTMR_ADDR(0x81);	//ch1
	OutTMR_DATA(0x1E);	//"0001 1110" EXP=0, MSA=30
	OutTMR_ADDR(0X82);	//ch2
	OutTMR_DATA(0x1F);	//"0001 1111" EXP=0, MSA=31
	// OutTMR_DATA(0X0F);	//MSA=15
	OutTMR_ADDR(0X83);	//ch3	128/83.3*30*22*31*32=1006.052us

	OutTMR_DATA(0x00);	//REQI, TGL, COI
	OutTMR_ADDR(0X90);	//CH0,1,2
	OutTMR_ADDR(0X91);
	OutTMR_ADDR(0X92);
	OutTMR_DATA(0X80);	//"1000 0000", REQI=1, interrupt enabled
	// OutTMR_DATA(0xE8);	//REQI=1 TGL=11 COI=83	PWM output at PC3
	OutTMR_ADDR(0x93);	//ch3

	OutTMR_DATA(0x00);
	OutTMR_ADDR(0x88);	//LCD=0, CPT_EN=0

	OutTMR_DATA(0X0F);	//set ch0,1,2,3 ON
	OutTMR_ADDR(0xA8);
}

void ADC_init()
{
	OrCRB(CRB7_index, 0x80);	//1000 0000  set adc_ref2V
	OutADC_CTRL(0x2E);			//adc_mode=10, prec=1, sel=110 ACH6
    OrIRQ0_EN(0x04);			//enable adc interrupt
}

static void timer_isr()
{

	timer_cnt++;

    //AndTMR_DATA(0x00);	// clear IFL register
	//OutTMR_ADDR(0xB8);
	//OrIRQ0_EN(0x02);	//open timer interrupt
}

static void ADC_isr()
{		  
/**************************************************************************/
/* Why OSIntNestingCtr already is 1 after the isr occurs, because it has interrupted the Timer interrupt
/* Are OS_ISR_ENTER(OS_CTX_SAVE) and OS_ISR_EXIT(OS_CTX_RESTORE) needed.
/* Ning said besides Timer, any other interrupts are unrelated? need to test
/*
/*
/**************************************************************************/


    //OS_ISR_ENTER;		   
    CPU_INT_DIS();


	//AndIRQ0_EN(0xFB);	//disable adc interrupt
		//ADC_Val[0] = InADA_DATH();	
		//ADC_Val[1] = InADA_DATL();

    adc_cnt++;

    //CPU_INT_DIS();
    //OrIRQ0_EN(0x04);	//enable adc interrupt,and reset its indictator
    //OSIntExit();
    CPU_INT_EN();
    //OS_ISR_EXIT;
    OrIRQ0_EN(0x04);
    asm("pop rar");
    asm("reti");
}

