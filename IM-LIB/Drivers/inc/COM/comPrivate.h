/* For use with UART 3 */

#ifndef __COMPRIVATE_H__
#define __COMPRIVATE_H__

//////////////////////////////////////////////////
// External UART
//
// Registers
#define UART_RHR	0			// Receiver holding register offset
#define UART_THR	0			// Transmitter holding register offset
#define UART_IER	1			// Interrupt enable register offset
#define UART_ISR	2			// Interrupt status register offset
#define UART_FCR	2			// FIFO control register
#define UART_LCR	3			// Line control register offset
#define UART_MCR	4			// Modem control register offset
#define UART_LSR	5			// Line status register offset
#define UART_MSR	6			// Modem status register offset
#define UART_SPR	7			// Scratchpad register offset
#define UART_DLL	0			// Divisor latch LSB register offset
#define UART_DLM	1			// Divisor latch MSB register offset

// Interrupt enable register
#define IER_RBF		0x01	    // Enable Receiver Buffer Full Interrupt
#define IER_TBE	    0x02	    // Enable Transmitter Buffer Empty Interrupt
#define IER_LS    	0x04	    // Enable Line Status Interrupt
#define IER_MS   	0x08	    // Enable Modem Status Interrupt

// Interrupt status register
#define ISR_ID1:	0x04		// Interrupt Identification bit 1
#define ISR_ID0:	0x02		// Interrupt Identification bit 0
#define ISR_PEND:	0x01		// Interrupt Pending

// Line control register
#define LCR_DLAB	0x80		// Divisor latch enable bit in LCR
#define LCR_BREAK	0x40		// Set break bit in LCR
#define LCR_NOPAR	0x00		// No parity in LCR
#define LCR_ODDPAR	0x08		// Odd parity in LCR
#define LCR_EVENPAR	0x18		// Even parity in LCR
#define LCR_1STOP	0x00		// 1  stop bit in LCR
#define LCR_2STOP	0x04		// 2 stop bits in LCR
#define LCR_5BIT	0x00		// 5 bit data in LCR
#define LCR_6BIT	0x01		// 6 bit data in LCR
#define LCR_7BIT	0x02		// 7 bit data in LCR
#define LCR_8BIT	0x03		// 8 bit data in LCR

// Modem Control Register
#define MCR_DTR		0x01
#define MCR_RTS		0x02

// Line status register 
#define LSR_DR		0x01		// Receiver buffer full flag in LSR (1=full)
#define LSR_OE		0x02		// Overrun error
#define LSR_PE		0x04		// Parity Error
#define LSR_FE		0x08		// Framing Error
#define LSR_BI		0x10		// Break interrupt
#define LSR_THRE	0x20		// Transmit holding register empty (new data can be written to transmit register)
#define LSR_TEMT	0x40		// Transmit empty (last byte has been sent)

// Modem status register
#define MSR_DCTS	0x01		// delta -CTS
#define MSR_DDSR	0x02		// delta -DSR
#define MSR_DRI		0x04		// delta -RI
#define MSR_DCD		0x08		// delta -CD
#define MSR_CTS	    0x10	    // CTS
#define MSR_DSR	   	0x20	    // DSR
#define MSR_RI	    0x40	    // RI
#define MSR_CD	    0x80	    // CD

///////////////////////////////////////////////////
// Serial port control block and settings struct
//
// Layout of the control block in I/O area.
// Do not change without making the corresponding changes to the microprogram!
// Do not change without making the corresponding changes to comdef.inc!
typedef struct {
	unsigned char	inbuf_outptr;
	unsigned char	outbuf_inptr;
	unsigned char	inbuf_inptr;
	unsigned char	outbuf_outptr;
	unsigned char	flow_flags;
} COMPORT_CBLK;

// flow_flags bit definitions
// Do not change without making the corresponding changes to the microprogram!
// Do not change without making the corresponding changes to comdef.inc!
#define FLOW_TXXON		0x01
#define FLOW_TXXOFF		0x03
//#define FLOW_XONXOFF	0x04
//#define FLOW_USED		0x08
#define FLOW_ASYMM		0x04
#define FLOW_RTSCTS		0x08
#define FLOW_XONXOFF	0x0C
#define FLOW_MODE		0x0C	//Must be bitwise OR of the three above
#define FLOW_OUT_OFF	0x10
#define FLOW_IN_OFF		0x20
#define FLOW_EVENT_RX	0x40	//These are used by the microprogram, make
#define FLOW_EVENT_TX	0x80    //sure they match the EVENT_SHIFT label below!

// Layout of the settings struct.
// Do not change without making the corresponding changes to comdef.inc!
typedef struct {
	int				control_reg;
	int				prescale_reg;
	int				status_reg;			// Status reg is used as enable flag for external ports
	int				data_reg;

	COMPORT_CBLK*	cblkp;	

	unsigned char*	inbuf_bufptr;
	unsigned char*	inbuf_inptr;
	unsigned char*	inbuf_outptr;
	unsigned char*	inbuf_endptr;

	unsigned char*	outbuf_bufptr;
	unsigned char*	outbuf_inptr;
	unsigned char*	outbuf_outptr;
	unsigned char*	outbuf_endptr;

	int				error_code;
	int				event_flags;

	int				handle;
	int				porttype;
	int				mode;
	const char		name[8];
	int				port;
	int				access;
	int				baudrate;
	int				databits;
	int				parity;
	int				stopbits;
	int				rcvTimeout;			// Receive timeout in ms
	int				rcvFramingByte;
	int 			rcvThreshold;
} COMPORT_SETTINGS;

// event_flags bit definitions
// Do not change without making the corresponding changes to the microprogram!
// Do not change without making the corresponding changes to comdef.inc!
#define EVENT_RX	0x01	
#define EVENT_TX	0x02
#define EVENT_OVR	0x04
#define EVENT_PRT	0x08
#define EVENT_FRM	0x10
#define EVENT_BRK	0x20
//This is the number of bits FLOW_EVENT_TX has to be shifted right to equal EVENT_TX
#define EVENT_SHIFT	6

//////////////////////////////////////////////////
// Other definitions
//
//Level limits
//Do not change without making the corresponding changes to the microprogram!
//Do not change without making the corresponding changes to comdef.inc!
#define FLOWOFF_LIMIT	0x20	//Level limit for stopping flow

//XON/XOFF characters. Don't change! Hardcoded
#define XON		0x11
#define XOFF	0x13

// And handle with HANDLEMASK to get port number
#define HANDLEMASK		0xFF		

// Base pointer to the I/O area
#ifdef _IM3000
#define CBLKP(n)		(COMPORT_CBLK*)(0xF10+sizeof(COMPORT_CBLK)*(n-1))		
#else
#define CBLKP(n)		(COMPORT_CBLK*)(0xF00+sizeof(COMPORT_CBLK)*(n-1))		
#endif

//////////////////////////////////////////////////
// Functions
//
COMPORT_SETTINGS* getComPortFromHandle(int handle);

int comGetNumPorts(void);
int comGetEventFlags(int handle);
void comSetEventFlags(int handle, int flags);

int comForceClose(int port);

int comSetReceiveTimeout(int handle, int millis);
int comGetReceiveTimeout(int handle);
int comSetReceiveThreshold(int handle, int thresh);	
int comGetReceiveThreshold(int handle);	

#endif //__COMPRIVATE_H__
