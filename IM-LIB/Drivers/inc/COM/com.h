//
// com.h
// 
// Copyright (C) 2015-2018 Cimsys Technologies. All rights reserved.
//
//

#ifndef __COM_H__
#define __COM_H__

// Error codes
#define E_COM_SUCCESS				 	 0
#define E_COM_INVALID_PARAM				-1
#define E_COM_PORT_ALREADY_OPEN			-2
#define E_COM_PORT_NOT_FOUND			-3
#define E_COM_OUT_OF_MEMORY				-4
#define E_COM_PRESCALER_NOT_SET			-5
#define E_COM_PORT_ALREADY_CLOSED		-6
#define E_COM_RX_FIFO_OVERRUN			-7	//Do not change, also defined elsewhere
#define E_COM_INPUT_BUFFER_OVERFLOW 	-8	//Do not change, also defined elsewhere
#define E_COM_PARITY_ERROR				-9	//Do not change, also defined elsewhere
#define E_COM_FRAMING_ERROR				-10	//Do not change, also defined elsewhere
#define E_COM_UNSUPPORTED_OPERATION		-11
#define E_COM_TIMEOUT					-12

// Port access modes
#define COM_READ			1
#define COM_WRITE			2
#define COM_READWRITE		3

// Parity modes
#define PARITY_NONE			0
#define PARITY_ODD			1
#define PARITY_EVEN			2

// Data bit modes
#define DATABITS_5			5
#define DATABITS_6			6
#define DATABITS_7			7
#define DATABITS_8			8

// Stop bit modes
#define STOPBITS_1			1
#define STOPBITS_2			2

// Flow control modes
#define FLOWCONTROL_NONE	0
#define FLOWCONTROL_RTSCTS	1
#define FLOWCONTROL_XONXOFF	2
#define FLOWCONTROL_ASYMM	3

#define BIT_RTS		0
#define BIT_CTS		1
#define BIT_DTR		2
#define BIT_DSR		3
#define BIT_CD		4
#define BIT_RI		5

// Functions
int comInit(void);
int comPortExists(int port);
const char *comGetPortName(int port);
int comOpen(int port, int baudrate, int databits, int stopbits, int parity, int flowcontrol, int access, int bufsize);
int comClose(int handle);

int comRead(int handle, unsigned char* buffer, int length);
int comWrite(int handle, const unsigned char* buffer, int length);

int comWaitData(int handle);
int comDataAvailable(int handle);
int comFlush(int handle);

int comConfig(int handle, int baudrate, int databits, int stopbits, int parity);

int comSetBaudrate(int handle, int baudrate);
int comGetBaudrate(int handle);

int comSetInputBufferSize(int handle, int size);
int comSetOutputBufferSize(int handle, int size);
int comGetInputBufferSize(int handle);
int comGetOutputBufferSize(int handle);

int comSetFlowControlMode(int handle, int mode);
int comGetFlowControlMode(int handle);

int comSetIrDAMode(int port, int enable);
int comSendBreak(int handle, int millis);

void comSetBit(int handle, int bit, int value);
int comGetBit(int handle, int bit);

int	comSetReceiveFraming(int handle, int framingByte);	
int	comGetReceiveFraming(int handle);	

void comSetExternalPortAdr(int portNum, int address);
int comGetExternalPortAdr(int portNum);
void comSetExternalPortEnable(int portNum, int enable);
int comGetExternalPortEnable(int portNum);

int comSetRTSCTSFlowControlEnable(int portNum, int enable);	
int comGetRTSCTSFlowControlEnable(int portNum);	

#endif //__COM_H__
