;
; comdef.inc
;
; Copyright (C) 2015-2018 Cimsys Technologies. Allrights reserved.
;
;===============================================================================

;===============================================================================
; External UARTS
;===============================================================================
; Register offsets
UART_RHR:	EQU	0		; Receiver holding register offset
UART_THR:	EQU	0		; Transmitter holding register offset
UART_IER:	EQU	1		; Interrupt enable register offset
UART_ISR:	EQU	2		; Interrupt status register offset
UART_FCR:	EQU	2		; FIFO control register
UART_LCR:	EQU	3		; Line control register offset
UART_MCR:	EQU	4		; Modem control register offset
UART_LSR:	EQU	5		; Line status register offset
UART_MSR:	EQU	6		; Modem status register offset
UART_SPR:	EQU	7		; Scratchpad register offset
UART_DLL:	EQU	0		; Divisor latch LSB register offset
UART_DLM:	EQU	1		; Divisor latch MSB register offset

;Interrupt enable register
IER_RBF:	EQU	0x01	    	; Enable Receiver Buffer Full Interrupt
IER_TBE:	EQU	0x02	    	; Enable Transmitter Buffer Empty Interrupt
IER_LS:		EQU	0x04	    	; Enable Line Status Interrupt
IER_MS:		EQU	0x08	    	; Enable Modem Status Interrupt

;Interrupt status register
ISR_ID1:	EQU	0x04	    	; Interrupt Identification bit 1
ISR_ID0:	EQU	0x02	    	; Interrupt Identification bit 0
ISR_PEND:	EQU	0x01	    	; Interrupt Pending

;Line control register
LCR_DLAB:	EQU	0x80		; Divisor latch enable bit in LCR
LCR_BREAK:	EQU	0x40		; Set break bit in LCR
LCR_NOPAR:	EQU	0x00		; No parity in LCR
LCR_ODDPAR:	EQU	0x08		; Odd parity in LCR
LCR_EVENPAR:	EQU	0x18		; Even parity in LCR
LCR_1STOP:	EQU	0x00		; 1  stop bit in LCR
LCR_2STOP:	EQU	0x04		; 2 stop bits in LCR
LCR_5BIT:	EQU	0x00		; 5 bit data in LCR
LCR_6BIT:	EQU	0x01		; 6 bit data in LCR
LCR_7BIT:	EQU	0x02		; 7 bit data in LCR
LCR_8BIT:	EQU	0x03		; 8 bit data in LCR

;Modem Control Register
MCR_DTR:	EQU	0x01	
MCR_RTS:	EQU	0x02	

;Line status register
LSR_DR:		EQU	0x01		; Receive Data Ready
LSR_OE:		EQU	0x02		; Overrun error
LSR_PE:		EQU	0x04		; Parity Error
LSR_FE:		EQU	0x08		; Framing Error
LSR_BI:		EQU	0x10		; Break interrupt
LSR_TRE:	EQU	0x20		; Transmitter Holding Register Empty
LSR_TSE:	EQU	0x40		; Transmitter Holding Shift Register Empty

;Modem Status Register
MSR_DCTS:	EQU	0x01		; delta -CTS
MSR_DDSR:	EQU	0x02		; delta -DSR
MSR_DRI:	EQU	0x04		; delta -RI
MSR_DCD:	EQU	0x08		; delta -CD
MSR_CTS:	EQU	0x10		; CTS
MSR_DSR:	EQU	0x20		; DSR
MSR_RI:		EQU	0x40		; RI
MSR_CD:		EQU	0x80		; CD

;===============================================================================
; Serial port control block and settings struct
;===============================================================================
;Layout of the control block in I/O area.
;Do not change without making the corresponding changes to the microprogram!
;Do not change without making the corresponding changes to comPrivate.h!
;typedef struct {
;	unsigned char	inbuf_outptr;
;	unsigned char	outbuf_inptr;
;	unsigned char	inbuf_inptr;
;	unsigned char	outbuf_outptr;
;	unsigned char	flow_flags;
;} COMPORT_CBLK;
INBUF_OUTPTR:		EQU	0
OUTBUF_INPTR:		EQU	1
INBUF_INPTR:		EQU	2
OUTBUF_OUTPTR:		EQU	3
FLOW_FLAGS:		EQU	4

;flow_flags bit definitions
;Do not change without making the corresponding changes to the microprogram!
;Do not change without making the corresponding changes to comPrivate.h!
FLOW_TXXON:		EQU	0x01    ;Don't change, hardcoded! Must be XON-XONOFF!
FLOW_TXXOFF:		EQU	0x03	;Don't change, hardcoded! Must be XOFF-XONOFF!
FLOW_TXXONOFF:		EQU	0x03	;Logic OR of the two above!
;FLOW_XONXOFF:		EQU	0x04
;FLOW_USED:		EQU	0x08
FLOW_ASYMM:		EQU	0x04
FLOW_RTSCTS:		EQU	0x08
FLOW_XONXOFF:		EQU	0x0C
FLOW_MODE:		EQU	0x0C	;Bitwise OR of the three above!
FLOW_OUT_OFF:		EQU	0x10
FLOW_IN_OFF:		EQU	0x20
FLOW_EVENT_RX:		EQU	0x40
FLOW_EVENT_TX:		EQU	0x80

;Layout of the settings struct.
;Do not change without making the corresponding changes to comPrivate.h!
;typedef struct {
;	int		control_reg;
;	int		prescale_reg;
;	int		status_reg;			// Status reg is used as enable flag for external ports
;	int		data_reg;
;
;	COMPORT_CBLK*	cblkp;	
;
;	unsigned char*	inbuf_bufptr;
;	unsigned char*	inbuf_inptr;
;	unsigned char*	inbuf_outptr;
;	unsigned char*	inbuf_endptr;
;
;	unsigned char*	outbuf_bufptr;
;	unsigned char*	outbuf_inptr;
;	unsigned char*	outbuf_outptr;
;	unsigned char*	outbuf_endptr;
;	
;	int		error_code;
;	int		event_flags;
;	
;	int		handle;
;	int		porttype;
;	int		mode;
;	const char	name[8];
;	int		port;
;	int		access;
;	int		baudrate;
;	int		databits;
;	int		parity;
;	int		stopbits;
;	int		rcvTimeout;			// Receive timeout in ms
;	int		rcvFramingByte;
;	int 		rcvThreshold;
;} COMPORT_SETTINGS;
CONTROL_REG:		EQU	3	;control_reg, offset to least significant byte
PRESCALE_REG:		EQU	7	;prescale_reg, offset to least significant byte
STATUS_REG:		EQU	11	;status_reg, offset to least significant byte
DATA_REG:		EQU	15	;data_reg, offset to least significant byte
CBLKP:			EQU	16
BIG_INBUF_BUFPTR:	EQU	20
BIG_INBUF_INPTR:	EQU	24
BIG_INBUF_OUTPTR:	EQU	28
BIG_INBUF_ENDPTR:	EQU	32
BIG_OUTBUF_BUFPTR:	EQU	36
BIG_OUTBUF_INPTR:	EQU	40
BIG_OUTBUF_OUTPTR:	EQU	44
BIG_OUTBUF_ENDPTR:	EQU	48
ERROR_CODE:		EQU	52
EVENT_FLAGS:		EQU	59	;event_flags, offset to least significant byte
PORT:			EQU     83	;port number, offset to least significant byte
SETTINGS_SZ:		EQU	116	;Size of the struct in bytes

;event_flags bit definitions
;Do not change without making the corresponding changes to the microprogram!
;Do not change without making the corresponding changes to comPrivate.h!
#define EVENT_RX	0x01
#define EVENT_TX	0x02
#define EVENT_OVR	0x04
#define EVENT_PRT	0x08
#define EVENT_FRM	0x10
#define EVENT_BRK	0x20

;error_code definitions
;Do not change without making the corresponding changes to com.h!
E_COM_RX_FIFO_OVERRUN:			EQU 	-7
E_COM_INPUT_BUFFER_OVERFLOW:		EQU 	-8
E_COM_PARITY_ERROR:			EQU	-9
E_COM_FRAMING_ERROR:			EQU	-10

;===============================================================================
; Other definitions
;===============================================================================
;Base pointers to the microprogrammed COM handler buffer areas
;Do not change without making the corresponding changes to the microprogram!
COM_IRQ_BASE:		EQU	0x1000	;Pointer to interrupt buffer
COM_BUF_BASE:		EQU	0x34	;ADH part of pointer to first data buffer

;Level limits
;Do not change without making the corresponding changes to the microprogram!
;Do not change without making the corresponding changes to comPrivate.h!
FLOWOFF_LIMIT:		EQU	0x20	;Level limit for stopping flow
RX_EMPTY_LIMIT:		EQU	0x80	;Level limit for emptying small input buffer
TX_FILL_LIMIT:		EQU	0x80	;Level limit for filling small output buffer

;XON/XOFF characters. Don't change! Hardcoded
XON:			EQU	0x11
XOFF:			EQU	0x13
XONOFF:			EQU	0x10

;Interrupt buffer pointers
IRQ_INPTR:		EQU	0xF50	;interrupt buffer input pointer
IRQ_OUTPTR:		EQU	0xF51	;interrupt buffer output pointer

;; COM port control block in I/O area
;COM_ISR:	EQU	0xF7F		;Interrupt select register
;COM_ISR_ADR:	EQU	0x0118
;;COM_ISR_MASK:	EQU	0x0200
;COM_IRQ_MASK:	EQU	0x02

;Peripheral mux control/interrupt mask register address
PMXC_IMR_addr:	equ	0x19

;Peripheral mux control/interrupt mask register bit definitions
IMR_MIRQ1_EN:	equ	0x02
IMR_MIRQ0_EN:	equ	0x01

;MIRQ0 control/status register address
MIRQ0_addr:	EQU	0x1A

;MIRQ0 control/status register bit definitions
MIRQ0_CAN:	equ	0x80
MIRQ0_MDI:	equ	0x40
MIRQ0_EXT:	equ	0x20
;MIRQ0_EDGE:	EQU	0x10
MIRQ0_CAN_EN:	equ	0x08
MIRQ0_MDI_EN:	equ	0x04
MIRQ0_EXT_EN:	equ	0x02
MIRQ0_ACK_EDGE:	EQU	0x01


