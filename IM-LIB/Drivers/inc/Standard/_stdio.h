/*
 * Standard C Library
 * (c) 1999, Astro Soft, St.Petersburg, Russia
 */

/*
 * $Header: /Compiler/C2GPx/Sources/ulib/crtl/include/_stdio.h 3     21.02.00 11:36 Vladimirp $
 */

# ifndef _CRTL_I_STDIO_H_INCLUDED
# define _CRTL_I_STDIO_H_INCLUDED

# define _IOBUF		0x0010	/* setvbuf called */
# define _IOALL		0x0020	/* malloc called */
# define _IOSTRT	0x0040	/* io started */

# define _IOBINARY	0x0080	/* binary file */
# define _IOTEXT	0x0100	/* text file */

# define _IOREAD	0x0200	/* opened for read or last read on update stream */
# define _IOWRITE	0x0400	/* opened for write or last write on update stream */
# define _IOPLUS	0x0800	/* opened for update ("+") */
# define _IOAPND	0x1000	/* opened for append */
# define _IOSEEK	0x2000	/* must seek end-of-file for append */

# define _IO_STDIN_DEFAULT	(_IOREAD | _IO_STDIN_LOCAL_DEFAULT)
# define _IO_STDOUT_DEFAULT	(_IOWRITE | _IO_STDOUT_LOCAL_DEFAULT)
# define _IO_STDERR_DEFAULT	(_IONBF | _IOWRITE | _IO_STDERR_LOCAL_DEFAULT)

# define _IO_STDIN_LOCAL_DEFAULT	(_IOLBF | _IOTEXT)
# define _IO_STDOUT_LOCAL_DEFAULT	(_IOLBF | _IOTEXT)
# define _IO_STDERR_LOCAL_DEFAULT	(_IOTEXT)

int _flush(FILE *);
void _iostrt(FILE *);
FILE *_findfile(void);
FILE *_fopen(const char *, const char *, FILE *);

int _printf(const char *, va_list, FILE *, char *);
int _scanf(const char *, va_list, FILE *, const char *);

# endif /* _CRTL_I_STDIO_H_INCLUDED */
