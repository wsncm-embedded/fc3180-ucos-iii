/*
 * Standard C Library
 * (c) 1999, Astro Soft, St.Petersburg, Russia
 */

/*
 *	_sys.h
 *
 */

/*
 * $Header: /Compiler/C2GPx/Sources/ulib/crtl/include/_sys.h 3     19.12.00 20:12 Vladimirp $
 */

# ifndef _CRTL__SYS_H_INCLUDED
# define _CRTL__SYS_H_INCLUDED

/* minimum page size that can be allocated */
# define SYS_PAGE_ALIGN	4 /* 256, 8192, ... */

# if _GP1000_ISAJ || _GP1000_ISAC
struct _timeb
{
	long time;
	int clocks;
};
# endif

# ifdef __cplusplus
extern "C" {
# endif

void _exit(int _Status);
void *_getsysblock(size_t _Size);
int _setsighandler(void (*)(int));
int _kill(int _Pid, int _Sig);
# if _GP1000_ISAJ || _GP1000_ISAC
void _ftime(struct _timeb *);
# endif

# ifdef __cplusplus
}
# endif
 
# endif /* _CRTL__SYS_H_INCLUDED */
