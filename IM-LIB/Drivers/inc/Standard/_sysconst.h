/*
 * Standard C Library
 * (c) 2000, Astro Soft, St.Petersburg, Russia
 */

/*
 *	_sysconst.h
 *
 */

/*
 * $Header: /Compiler/C2GPx/Sources/ulib/crtl/include/_sysconst.h 1     19.12.00 20:08 Vladimirp $
 */

# ifndef _CRTL__SYSCONST_H_INCLUDED
# define _CRTL__SYSCONST_H_INCLUDED

# define __TIMER_FREQ	60
# define __TIMER_IRQ	0

# endif /* _CRTL__SYSCONST_H_INCLUDED */
