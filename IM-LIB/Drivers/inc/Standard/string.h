#ifndef _CRTL_STRING_H_INCLUDED
#define _CRTL_STRING_H_INCLUDED
/*
** File:     string.h
**
** Contents: Standard C Library.
**
** Version:  1.0
**
** Copyright 2015-2018 Cimsys Technologies.
**           All Rights Reserved
**
**
**	7.21 String handling
**	7.21.1 String function conventions
**	7.21.2 Copying functions
**	7.21.3 Concatenation functions
**	7.21.4 Comparison functions
**	7.21.5 Search functions
**	7.21.6 Miscellaneous functions
*/

#ifndef _CRTL_SIZE_T_DEFINED
#define _CRTL_SIZE_T_DEFINED
typedef unsigned size_t;
#endif

#ifndef NULL
#ifdef __cplusplus
#define NULL	0
#else
#define NULL	((void *)0)
#endif
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* 7.21.2 Copying functions */

void *memcpy(void *_S1, const void *_S2, size_t _N);
void *memmove(void *_S1, const void *_S2, size_t _N);
char *strcpy(char *_S1, const char *_S2);
char *strncpy(char *_S1, const char *_S2, size_t _N);

/* 7.21.3 Concatenation functions */

char *strcat(char *_S1, const char *_S2);
char *strncat(char *_S1, const char *_S2, size_t _N);

/* 7.21.4 Comparison functions */

int memcmp(const void *_S1, const void *_S2, size_t _N);
int strcmp(const char *_S1, const char *_S2);
int strcoll(const char *_S1, const char *_S2);
int strncmp(const char *_S1, const char *_S2, size_t _N);
size_t strxfrm(char *_S1, const char *_S2, size_t _N);

/* 7.21.5 Search functions */

void *memchr(const void *_S, int _C, size_t _N);
char *strchr(const char *_S, int _C);
size_t strcspn(const char *s1, const char *s2);
char *strpbrk(const char *_S1, const char *_S2);
char *strrchr(const char *_S, int _C);
size_t strspn(const char *_S1, const char *_S2);
char *strstr(const char *_S1, const char *_S2);
char *strtok(char *_S1, const char *_S2);

/* 7.21.6 Miscellaneous functions */

void *memset(void *_S, int _C, size_t _N);
char *strerror(int _Errnum);
size_t strlen(const char *_S);

#ifdef __cplusplus
}
#endif
 
#endif /* _CRTL_STRING_H_INCLUDED */
