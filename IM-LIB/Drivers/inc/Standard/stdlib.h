#ifndef _CRTL_STDLIB_H_INCLUDED
#define _CRTL_STDLIB_H_INCLUDED
/*
** File:     stdlib.h
**
** Contents: Standard C Library.
**
** Version:  1.0
**
** Copyright 2015-2018 Cimsys Technologies.
**           All Rights Reserved
**
**
**	7.20 General utilities
**	7.20.1 String conversion functions
**	7.20.2 Pseudo-random sequence generation functions
**	7.20.3 Memory management functions
**	7.20.4 Communication with the environment
**	7.20.5 Searching and sorting utilities
**	7.20.6 Integer arithmetic functions
**	7.20.7 Multibyte character functions
**	7.20.8 Multibyte string functions
*/

#ifndef _CRTL_SIZE_T_DEFINED
#define _CRTL_SIZE_T_DEFINED
typedef unsigned size_t;
#endif

#ifndef _CRTL_WCHAR_T_DEFINED
#define _CRTL_WCHAR_T_DEFINED
#ifndef __cplusplus
typedef unsigned short wchar_t;
#endif
#endif

//typedef struct _DIV { int quot, rem; } div_t;
//typedef struct _LDIV { long quot,rem; } ldiv_t;

#ifndef NULL
#ifdef __cplusplus
#define NULL	0
#else
#define NULL	((void *)0)
#endif
#endif

#define EXIT_FAILURE 1
#define EXIT_SUCCESS 0

#define RAND_MAX	32767
#define MB_CUR_MAX	1

#ifdef __cplusplus
extern "C" {
#endif

//double atof(const char *_Nptr);
//int atoi(const char *_Nptr);
//long int atol(const char *_Nptr);
//double strtod(const char *_Nptr, char **_Endptr);
//long int strtol(const char *_Nptr, char **_Endptr, int _Base);
//unsigned long int strtoul(const char *_Nptr, char **_Endptr, int _Base);

//int rand(void);
//void srand(unsigned int _Seed);

void *calloc(size_t _Nmemb, size_t _Size);
void free(void *_Ptr);
void *malloc(size_t _Size);
void *realloc(void *_Ptr, size_t _Size);

//void abort(void);
//int atexit(void (*_Func)(void));
//void exit(int _Status);

//char *getenv(const char *_Name);
//int setenv(const char *name, const char *value, int overwrite);
//int putenv( const char *str );
//void unsetenv(const char *name);
//int system(const char *string);

//void *bsearch(const void *_Key, const void *_Base, size_t _Nmemb, size_t _Size, int (*_Compar)(const void *, const void *));
//void qsort(void *_Base, size_t _Nmemb, size_t _Size, int (*_Compar)(const void *, const void *));

//int abs(int _J);
//long int labs(long _J);
//div_t div(int _Numer, int _Denom);
//ldiv_t ldiv(long _Numer, long _Denom);

//int mblen(const char *_S, size_t _N);
//int mbtowc(wchar_t *_Pwc, const char *_S, size_t _N);
//int wctomb(char *_S, wchar_t _Wchar);
//size_t mbstowcs(wchar_t *_Pwcs, const char *_S, size_t _N);
//size_t wcstombs(char *_S, const wchar_t *_Pwcs, size_t _N);

#ifdef __cplusplus
}
#endif
 
#endif /* _CRTL_STDLIB_H_INCLUDED */
