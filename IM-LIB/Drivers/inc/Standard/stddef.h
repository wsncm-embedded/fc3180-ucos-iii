#ifndef _CRTL_STDDEF_H_INCLUDED
#define _CRTL_STDDEF_H_INCLUDED
/*
** File:     stdlib.h
**
** Contents: Standard C Library.
**
** Version:  1.0
**
** Copyright 2015-2018 Cimsys Technologies.
**           All Rights Reserved
**
**	7.17 Common definitions
*/

#ifndef _CRTL_PTRDIFF_T_DEFINED
#define _CRTL_PTRDIFF_T_DEFINED
typedef int ptrdiff_t;
#endif

#ifndef _CRTL_SIZE_T_DEFINED
#define _CRTL_SIZE_T_DEFINED
typedef unsigned size_t;
#endif

#ifndef _CRTL_SSIZE_T_DEFINED
#define _CRTL_SSIZE_T_DEFINED
typedef int ssize_t;
#endif

#ifndef _CRTL_OFF_T_DEFINED
#define _CRTL_OFF_T_DEFINED
typedef int off_t;
#endif

#ifndef _CRTL_WCHAR_T_DEFINED
#define _CRTL_WCHAR_T_DEFINED
#ifndef __cplusplus
typedef unsigned short wchar_t;
#endif
#endif

#ifndef NULL
#ifdef __cplusplus
#define NULL	0
#else
#define NULL	((void *)0)
#endif
#endif

#define offsetof(t,m)	((size_t)((char *)&((t *)0)->m - (char *)0))

#endif /* _CRTL_STDDEF_H_INCLUDED */
