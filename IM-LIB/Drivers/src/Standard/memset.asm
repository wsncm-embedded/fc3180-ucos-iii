;===============================================================================
;
; FILE: memset.asm
;
; Copyright (C) 2000 Imsys AB. All rights reserved
;
;===============================================================================
; 
; Assembly replacement for Standard C Library function 
;
; C syntax:	void *memset(void *_S, int _C, size_t _N);
;
;===============================================================================

	SEGMENT ctrl,CODE


_memset::
	dup
	push	es		; s
	nrot			; ..., s, size, char
	fill
	pop	es		; ..., s
	ret

