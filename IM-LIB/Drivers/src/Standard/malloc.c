/*
 * Standard C Library
 * (c) 1999, Astro Soft, St.Petersburg, Russia
 */

/* 
 * malloc.c 
 */ 

/*
 * $Header: /Compiler/C2GPx/Sources/ulib/crtl/stdlib/malloc.c 3     21.02.00 11:36 Vladimirp $
 */

#include "stdlib.h"
#include "string.h"
#include "_sys.h"
#include <cpu.h>

#define add_entry(p, q, r)
#define remove_entry(p)


typedef struct header
{
	struct header *ptr;
	size_t size;
} _Header;

static _Header _base;
static _Header *_freep = NULL;

int alloced = 0;

void __free(void *);
#undef malloc

#define NALLOC 1024

static _Header *morecore(size_t nu)
{
	char *cp;
	_Header *up;
	size_t n;

	if (nu < NALLOC)
		nu = NALLOC;
	if (n = (nu * sizeof(_Header) % SYS_PAGE_ALIGN))
		nu += (SYS_PAGE_ALIGN - n) / sizeof(_Header);
	cp = _getsysblock(nu * sizeof(_Header));
	if (cp == NULL)
		return NULL;
	up = (_Header *)cp;
	up->size = nu;
	__free((void *)(up + 1));
	return _freep;
}

/*
	Storage allocation functions:
*/

void *(malloc)(size_t nbytes)
{
	_Header *p, *prevp;
	unsigned nunits;

//	int rar = (int)asm("ld rar");

	nunits = (nbytes + (sizeof _base - 1)) / sizeof _base + 1;

	CPU_CRITICAL_ENTER();


	if ((prevp = _freep) == NULL) {		/* no free list yet. */
		_base.ptr = _freep = prevp = & _base;
		_base.size = 0;
	}
	for (p = prevp->ptr;; prevp = p, p = p->ptr) 
	{
		if (p->size >= nunits)		/* big enough. */
		{	if (p->size == nunits)	/* exactly */
				prevp->ptr = p->ptr;
			else 
			{
				p->size -= nunits;
				p += p->size;
				p->size = nunits;
			}
			_freep = prevp;
			alloced += nunits;

			add_entry( p+1, rar, nbytes );


//	memset(p+1, 0x99, nbytes);

			CPU_CRITICAL_EXIT();
			return p + 1;
		}
		if (p == _freep) 
		{
			if ((p = morecore(nunits)) == NULL) {

				CPU_CRITICAL_EXIT();
				return NULL;
		        }
		}
	}
}

void (free)(void *ap)
{

	_Header *bp;

	bp = (_Header *)ap - 1;	/* point to block header. */


	alloced -= bp->size;

	CPU_CRITICAL_ENTER();
//	memset(ap, 0xEE, (bp->size-1)*8)

    remove_entry( ap );
    __free( ap );

	CPU_CRITICAL_EXIT();
}

void (__free)(void *ap)
{	
	_Header *bp, *p;

	if (ap == NULL)
		return;
	bp = (_Header *)ap - 1;	/* point to block header. */

	for (p = _freep; !(bp > p && bp < p->ptr); p = p->ptr)
		if (p >= p->ptr && (bp > p || bp < p->ptr))
			break;		/* freed block at start or end of arena. */
	if (bp + bp->size == p->ptr) {
		bp->size += p->ptr->size;
		bp->ptr = p->ptr->ptr;
	} else 
		bp->ptr = p->ptr;
	if (p + p->size == bp) {
		p->size += bp->size;
		p->ptr = bp->ptr;
	}
	else 
		p->ptr = bp;


	_freep = p;
}

void *(realloc)(void *ap, size_t nbytes)
{
	_Header *bp, *p;
	unsigned nunits; 

	if (ap == NULL)
		return malloc(nbytes);
	bp = (_Header *)ap - 1;	/* point to block header. */
	nunits = (nbytes + (sizeof _base - 1)) / sizeof _base + 1;
	if (nunits <= bp->size)
		return ap;
	p = malloc(nbytes);
	if (p == NULL)
		return NULL;
	memcpy(p, ap, (bp->size - 1) * sizeof _base);
	free(ap);
	return p;
}
