;===============================================================================
;
; FILE: memcpy.asm
;
; Copyright (C) 2000 Imsys AB. All rights reserved
;
;===============================================================================
; 
; Assembly replacement for Standard C Library function 
;
; C syntax:	void *memcpy(void *_S1, const void *_S2, size_t _N);
;
;===============================================================================
; 
;  Author:	Joakim Jald�n, Imsys AB
;  
;  Date:	20000707		Created
;		20000913	PA	Optimized
;
;=============================================================================

	SEGMENT memcpy,CODE

_memcpy::
	dup
	push	es	; s1
	swap
        rot		; ..., s1, s2, size
        bcopy
	pop	es	; ..., s1
        ret

