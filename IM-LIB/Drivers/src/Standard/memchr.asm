;===============================================================================
;
; FILE: memchr.asm
;
; Copyright (C) 2000 Imsys AB. All rights reserved
;
;===============================================================================
; 
; Assembly replacement for Standard C Library function 
;
; C syntax:	void *memchr(const void *_S, int _C, size_t _N);
;
;===============================================================================
; 
;  Author:	Joakim Jald�n, Imsys AB
;  
;  Date:	20000821		Created
;		20001004	PA	Updated
;
;=============================================================================

	SEGMENT memchr,CODE


_memchr::
        nrot		; ..., s, size, char
	scan		; ..., s | 0
        ret
