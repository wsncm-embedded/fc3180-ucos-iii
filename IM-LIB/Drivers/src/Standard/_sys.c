/*
 * Standard C Library
 * (c) 1999, Astro Soft, St.Petersburg, Russia
 */

/* 
 * _sys.c
 * 
 * low-level functions stubs
 */ 

/*
 * $Header: /Compiler/C2GPx/Sources/ulib/crtl/stdlib/_sys.c 3     19.12.00 20:12 Vladimirp $
 */

# include "stddef.h"
# include "_sys.h"
# include "_sysconst.h"

/*
 *	Machine - dependent subroutinues
 */

extern char *_heapbottom;
extern char *_heaptop;
extern char *_heapstart;

int _brk(char *newp)
{
	if (newp >= _heaptop || newp < _heapbottom)
		return -1;
	_heapstart = newp;
	return 0;
}

void *_sbrk(size_t heapreq)
{	
	char *newtop, *oldtop;

	return ((newtop = (oldtop = _heapstart) + heapreq) < _heapstart ||
		_brk(newtop)) ? (void *) (-1) : oldtop;
}

/*
 *	Interface routinues
 */

void *_getsysblock(size_t sz)
{
	char *cp = _sbrk(sz);
	if (cp == (char *)-1)	/* No space at all */
		return NULL;
	return cp;
}

#pragma warn variables off
int _setsighandler(void (*sighandler)(int))
{
	return -1;
}

int	_kill(int pid, int sig)
{
	return -1;
}
#pragma warn variables on

#if _GP1000_ISAJ || _GP1000_ISAC
extern unsigned char sys_timer_mask;
extern unsigned short sys_timer_reload;
extern void (*sys_int_base[])(void);
static long _time_cntr;
static long _time_clks;

void _ftime(struct _timeb *p)
{
	p->time = _time_cntr;
	p->clocks = _time_clks;
}

# endif
/*
# if _GP1000_ISAJ || _GP1000_ISAC
extern void _sigillMessage(int n, const char *p);

void _sig_ill(const char *pc)
{
	pc -= 2;
	if (*pc != 0xFC && *pc != 0xFE && *pc != 0xC4)
		_sigillMessage(1, ++pc);
	else
		_sigillMessage(2, pc);
}
# endif
*/
