//
// com.c
// 
// Copyright (C) 2003 Imsys AB. All rights reserved.
//
//
// Revision history:
// ----------------------------------------------------------------------------
// 2004-07-15	AG		Added support for 8 external serial ports.
// 2005-01-20	CB		Fixed bugs in hardware flow control.
// 2005-02-25	CB		Fixed fatal internal port bug, caused by fix above.
// 2008-09-01	CB		Added the asymmetric RTS/CTS flow control mode.
//
//

/*******************************************************************************
* @module	COM 
*	
* @desc		Asynchronous serial port driver.
*
* @header	com.h
* @author	Imsys Technologies
*	
*******************************************************************************/


//#include <rubus.h>
/*uCOS support*/
#include "os.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <io_macro.h>
#include "prio_level.h"
#include "system.h"
//#include "basic\jos_event.h"
#include "uart_defs.h"
#include "com.h"
#include "comPrivate.h"
#include "sys_defs.h"

//-- IM3000 ---------------------------------------------------------------------
#ifdef _IM3000
#define NUMCOMPORTS		3		// IM3000 supports 3 internal ports
#else	
//-- IM1000 ---------------------------------------------------------------------
#ifdef SNAP
#ifdef _BOOTLOADER
#define NUMCOMPORTS		2		// SNAP boot loader supports 2 ports
#else
#define NUMCOMPORTS		16		// SNAP supports 16 ports, both internal and external 
#endif
#else
#define NUMCOMPORTS		16		// IM1000 standard supports 16 internal ports
#endif
#endif
//-----------------------------------------------------------------------

#define MINBUFSIZE		256			// Smallest allowed size of in and out buffers

#define MODE_NORMAL		0
#define MODE_IRDA		1
						   
#define DELAY_RECV_DATA_MS		10

//#define TRACE	

#ifdef TRACE
#define TRACE0(_s)					printf(_s)
#define TRACE1(_s, _p0)				printf(_s, _p0)
#define TRACE2(_s, _p0, _p1)		printf(_s, _p0, _p1)
#define TRACE3(_s, _p0, _p1, _p2)	printf(_s, _p0, _p1, _p2)
#else
#define TRACE0(_s)
#define TRACE1(_s, _p0)
#define TRACE2(_s, _p0, _p1)
#define TRACE3(_s, _p0, _p1, _p2)
#endif

// Port types
#define INTERNAL_PORT		0
#define EXTERNAL_PORT		1

//#pragma inline on
#define inline

static int hwhandshakeOwner = -1;    
#ifdef SNAP
// Base address for external ports
#define DEF_BASEADR_SERIAL2	  	0x380020
#define DEF_BASEADR_SERIAL3	  	0x380028
#define DEF_BASEADR_SERIAL6	  	0x380030
#define DEF_BASEADR_SERIAL7	  	0x380038
#define DEF_BASEADR_SERIAL10	0x380040
#define DEF_BASEADR_SERIAL11	0x380048
#define DEF_BASEADR_SERIAL14	0x380050
#define DEF_BASEADR_SERIAL15	0x380058

static int hwhandshakeOwner = -1;    

// Pin OUT2 = B.7 
#define SET_DTR()		{ OrCB(EXP_OUT2); AndDB(~EXP_OUT2); }
#define RESET_DTR()		{ OrCB(EXP_OUT2); OrDB(EXP_OUT2); }
#define READ_DTR(b)		b = !(InDB() & EXP_OUT2);
// Pin IN3 = B.0 
#define READ_CD(b)		{ AndCB(~EXP_IN3); b = !(InDB() & EXP_IN3); }

#endif 

#define RESET_RTS()		OrB(pcs->control_reg, UCR_RTS);
#define SET_RTS()		AndB(pcs->control_reg, ~UCR_RTS);
#define READ_RTS(b)		b = !(InB(pcs->control_reg) & UCR_RTS);
    			
#define READ_CTS(b)		b = !(InB(pcs->control_reg) & UCR_CTS);

// Prescale values
#define PRESCALE_300		0
#define PRESCALE_600		1
#define PRESCALE_1200		2
#define PRESCALE_2400		3
#define PRESCALE_4800	    4
#define PRESCALE_7200	    5
#define PRESCALE_9600		6
#define PRESCALE_14400	    7
#define PRESCALE_19200	    8
#define PRESCALE_28800	    9
#define PRESCALE_38400	   10
#define PRESCALE_57600	   11
#define PRESCALE_115200	   12
#define PRESCALE_230400	   13 
#define PRESCALE_460800	   14
#define PRESCALE_921600	   15

//#define DUMPDATA

#ifdef DUMPDATA
static char hex[16] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
#endif

#ifdef SNAP
// Devisor values for external UART
// Baudrate (3.6864 MHz / baudrate / 16 = 2)
static const unsigned short prescale_ext[16] = {768,	// 300	   
											    384,	// 600		
											    192,	// 1200	 
											    96, 	// 2400	
											    48, 	// 4800	 
											    32,		// 7200		
											    24, 	// 9600
											    16, 	// 14400  
											    12, 	// 19200
											    8,		// 28800	
											    6, 	    // 38400
											    4, 		// 57600
											    2, 		// 115200
											    1,		// 230400
											    0,	    // 460800   N/A
											    0};		// 921600   N/A
#endif

// Devisor values for internal UART
static const unsigned short prescale_int[16] = {UIR_BAUD_300, 		// 300		
											    UIR_BAUD_600,		// 600		
											    UIR_BAUD_1200,		// 1200	 
											    UIR_BAUD_2400, 		// 2400	
											    UIR_BAUD_4800, 		// 4800	 
											    UIR_BAUD_7200,		// 7200		
											    UIR_BAUD_9600, 		// 9600
											    UIR_BAUD_14400, 	// 14400  
											    UIR_BAUD_19200, 	// 19200
											    UIR_BAUD_28800,		// 28800	
											    UIR_BAUD_38400,		// 38400
											    UIR_BAUD_57600, 	// 57600
											    UIR_BAUD_115200, 	// 115200
											    UIR_BAUD_230400,	// 230400
											    UIR_BAUD_460800,	// 460800   
											    UIR_BAUD_921600};	// 921600   


#define FIRST_PORT		1
#define LAST_PORT		NUMCOMPORTS

//-----------------------------------------------------------------------------
#ifdef _IM3000
#ifdef SNAPG
COMPORT_SETTINGS	ComPort1 	 = {
								    COM1_CONTROL_REG,
								    COM1_PRESCALE_REG,
								    COM1_STATUS_REG,
								    COM1_DATA_REG,
									CBLKP(1),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "serial0",					// port name
								    1};						// port num			

COMPORT_SETTINGS	ComPort2 	 = {
								    COM2_CONTROL_REG,
								    COM2_PRESCALE_REG,
								    COM2_STATUS_REG,
								    COM2_DATA_REG,
									CBLKP(2),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "serial1",					// port name
								    2};						// port num			

COMPORT_SETTINGS	ComPort3 	 = {
								    COM3_CONTROL_REG,
								    COM3_PRESCALE_REG,
								    COM3_STATUS_REG,
								    COM3_DATA_REG,
									CBLKP(3),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "serial4",					// port name
								    3};						// port num			

COMPORT_SETTINGS	ComPort4 	 = {
								    COM4_CONTROL_REG,
								    COM4_PRESCALE_REG,
								    COM4_STATUS_REG,
								    COM4_DATA_REG,
									CBLKP(4),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "serial2",					// port name
								    4};						// port num			

COMPORT_SETTINGS	ComPort5 	 = {
								    COM5_CONTROL_REG,
								    COM5_PRESCALE_REG,
								    COM5_STATUS_REG,
								    COM5_DATA_REG,
									CBLKP(5),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "serial3",				// port name
								    5};						// port num			

COMPORT_SETTINGS	ComPort6 	 = {
								    COM6_CONTROL_REG,
								    COM6_PRESCALE_REG,
								    COM6_STATUS_REG,
								    COM6_DATA_REG,
									CBLKP(6),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM6",					// port name
								    6};						// port num			

COMPORT_SETTINGS	ComPort7 	 = {
								    COM7_CONTROL_REG,
								    COM7_PRESCALE_REG,
								    COM7_STATUS_REG,
								    COM7_DATA_REG,
									CBLKP(7),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM7",					// port name
								    7};						// port num			

COMPORT_SETTINGS	ComPort8 	 = {
								    COM8_CONTROL_REG,
								    COM8_PRESCALE_REG,
								    COM8_STATUS_REG,
								    COM8_DATA_REG,
									CBLKP(8),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM8",					// port name
								    8};						// port num			

COMPORT_SETTINGS	ComPort9 	 = {
								    COM9_CONTROL_REG,
								    COM9_PRESCALE_REG,
								    COM9_STATUS_REG,
								    COM9_DATA_REG,
									CBLKP(9),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM9",					// port name
								    9};						// port num			

COMPORT_SETTINGS	ComPort10	 = {
								    COM10_CONTROL_REG,
								    COM10_PRESCALE_REG,
								    COM10_STATUS_REG,
								    COM10_DATA_REG,
									CBLKP(10),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM10",				// port name
								    10};					// port num			

COMPORT_SETTINGS	ComPort11	 = {
								    COM11_CONTROL_REG,
								    COM11_PRESCALE_REG,
								    COM11_STATUS_REG,
								    COM11_DATA_REG,
									CBLKP(11),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM11",				// port name
								    11};					// port num			

COMPORT_SETTINGS	ComPort12	 = {
								    COM12_CONTROL_REG,
								    COM12_PRESCALE_REG,
								    COM12_STATUS_REG,
								    COM12_DATA_REG,
									CBLKP(12),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM12",				// port name
								    12};					// port num			

COMPORT_SETTINGS	ComPort13	 = {
								    COM13_CONTROL_REG,
								    COM13_PRESCALE_REG,
								    COM13_STATUS_REG,
								    COM13_DATA_REG,
									CBLKP(13),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM13",				// port name
								    13};					// port num			

COMPORT_SETTINGS	ComPort14	 = {
								    COM14_CONTROL_REG,
								    COM14_PRESCALE_REG,
								    COM14_STATUS_REG,
								    COM14_DATA_REG,
									CBLKP(14),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM14",				// port name
								    14};					// port num			

COMPORT_SETTINGS	ComPort15	 = {
								    COM15_CONTROL_REG,
								    COM15_PRESCALE_REG,
								    COM15_STATUS_REG,
								    COM15_DATA_REG,
									CBLKP(15),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM15",				// port name
								    15};					// port num			

COMPORT_SETTINGS	ComPort16	 = {
								    COM16_CONTROL_REG,
								    COM16_PRESCALE_REG,
								    COM16_STATUS_REG,
								    COM16_DATA_REG,
									CBLKP(16),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM16",				// port name
								    16};					// port num	
#else
// IM3000 supports 3 internal ports
COMPORT_SETTINGS	ComPort1 	 = {
								    COM1_CONTROL_REG,
								    COM1_PRESCALE_REG,
								    COM1_STATUS_REG,
								    COM1_DATA_REG,
									CBLKP(1),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM1",					// port name
								    1};						// port num			

COMPORT_SETTINGS	ComPort2 	 = {
								    COM2_CONTROL_REG,
								    COM2_PRESCALE_REG,
								    COM2_STATUS_REG,
								    COM2_DATA_REG,
									CBLKP(2),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM2",					// port name
								    2};						// port num			

COMPORT_SETTINGS	ComPort3 	 = {
								    COM3_CONTROL_REG,
								    COM3_PRESCALE_REG,
								    COM3_STATUS_REG,
								    COM3_DATA_REG,
									CBLKP(3),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM3",					// port name
								    3};						// port num			
#endif
//-----------------------------------------------------------------------------


#else	// IM1000

#ifdef SNAP
// SNAP
// Internal and external serial ports

COMPORT_SETTINGS	ComPort1 	 = {
								    COM1_CONTROL_REG,
								    COM1_PRESCALE_REG,
								    COM1_STATUS_REG,
								    COM1_DATA_REG,
									CBLKP(1),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "serial1",				// port name
								    1};						// port num			

COMPORT_SETTINGS	ComPort2 	 = {
								    COM2_CONTROL_REG,
								    COM2_PRESCALE_REG,
								    COM2_STATUS_REG,
								    COM2_DATA_REG,
									CBLKP(2),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "serial0",				// port name
								    2};						// port num			

#ifndef _BOOTLOADER
COMPORT_SETTINGS	ComPort3 	 = {
								    COM3_CONTROL_REG,
								    COM3_PRESCALE_REG,
								    COM3_STATUS_REG,
								    COM3_DATA_REG,
									CBLKP(3),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "serial4",				// port name
								    3};						// port num			

COMPORT_SETTINGS	ComPort4 	 = {
								    DEF_BASEADR_SERIAL2,	// Base address for external UART
								    0,
								    0,						// Status reg is used as enable flag for external ports
								    0,
									CBLKP(4),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									EXTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "serial2",				// port name
								    4};						// port num			

COMPORT_SETTINGS	ComPort5 	 = {
								    DEF_BASEADR_SERIAL3,	// Base address for external UART
								    0,
								    0,						// Status reg is used as enable flag for external ports
								    0,
									CBLKP(5),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									EXTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "serial3",				// port name
								    5};						// port num			


COMPORT_SETTINGS	ComPort6 = {0};		// Reserved for internal port serial5

COMPORT_SETTINGS	ComPort7 	 = {
								    DEF_BASEADR_SERIAL6,	// Base address for external UART
								    0,
								    0,						// Status reg is used as enable flag for external ports
								    0,
									CBLKP(7),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									EXTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "serial6",				// port name
								    7};						// port num			

COMPORT_SETTINGS	ComPort8 	 = {
								    DEF_BASEADR_SERIAL7,	// Base address for external UART
								    0,
								    0,						// Status reg is used as enable flag for external ports
								    0,
									CBLKP(8),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									EXTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "serial7",				// port name
								    8};						// port num			

COMPORT_SETTINGS	ComPort9 = {0};			// Reserved for internal port serial8

COMPORT_SETTINGS	ComPort10 = {0};		// Reserved for internal port serial9

COMPORT_SETTINGS	ComPort11 	 = {
								    DEF_BASEADR_SERIAL10,	// Base address for external UART
								    0,
								    0,						// Status reg is used as enable flag for external ports
								    0,
									CBLKP(11),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									EXTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "serial10",				// port name
								    11};					// port num			

COMPORT_SETTINGS	ComPort12 	 = {
								    DEF_BASEADR_SERIAL11,	// Base address for external UART
								    0,
								    0,						// Status reg is used as enable flag for external ports
								    0,
									CBLKP(12),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									EXTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "serial11",				// port name
								    12};					// port num			

COMPORT_SETTINGS	ComPort13 = {0};		// Reserved for internal port serial12

COMPORT_SETTINGS	ComPort14 = {0};		// Reserved for internal port serial13

COMPORT_SETTINGS	ComPort15 	 = {
								    DEF_BASEADR_SERIAL14,	// Base address for external UART
								    0,
								    0,						// Status reg is used as enable flag for external ports
								    0,
									CBLKP(15),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									EXTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "serial14",				// port name
								    15};					// port num			

COMPORT_SETTINGS	ComPort16 	 = {
								    DEF_BASEADR_SERIAL15,	// Base address for external UART
								    0,
								    0,						// Status reg is used as enable flag for external ports
								    0,
									CBLKP(16),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									EXTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "serial15",				// port name
								    16};					// port num			
#endif 	// _BOOTLOADER

#else
// EVKIT / COMKIT
// Internal serial ports only
COMPORT_SETTINGS	ComPort1 	 = {
								    COM1_CONTROL_REG,
								    COM1_PRESCALE_REG,
								    COM1_STATUS_REG,
								    COM1_DATA_REG,
									CBLKP(1),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM1",					// port name
								    1};						// port num			

COMPORT_SETTINGS	ComPort2 	 = {
								    COM2_CONTROL_REG,
								    COM2_PRESCALE_REG,
								    COM2_STATUS_REG,
								    COM2_DATA_REG,
									CBLKP(2),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM2",					// port name
								    2};						// port num			

COMPORT_SETTINGS	ComPort3 	 = {
								    COM3_CONTROL_REG,
								    COM3_PRESCALE_REG,
								    COM3_STATUS_REG,
								    COM3_DATA_REG,
									CBLKP(3),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM3",					// port name
								    3};						// port num			

COMPORT_SETTINGS	ComPort4 	 = {
								    COM4_CONTROL_REG,
								    COM4_PRESCALE_REG,
								    COM4_STATUS_REG,
								    COM4_DATA_REG,
									CBLKP(4),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM4",					// port name
								    4};						// port num			

COMPORT_SETTINGS	ComPort5 	 = {
								    COM5_CONTROL_REG,
								    COM5_PRESCALE_REG,
								    COM5_STATUS_REG,
								    COM5_DATA_REG,
									CBLKP(5),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM5",					// port name
								    5};						// port num			

COMPORT_SETTINGS	ComPort6 	 = {
								    COM6_CONTROL_REG,
								    COM6_PRESCALE_REG,
								    COM6_STATUS_REG,
								    COM6_DATA_REG,
									CBLKP(6),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM6",					// port name
								    6};						// port num			

COMPORT_SETTINGS	ComPort7 	 = {
								    COM7_CONTROL_REG,
								    COM7_PRESCALE_REG,
								    COM7_STATUS_REG,
								    COM7_DATA_REG,
									CBLKP(7),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM7",					// port name
								    7};						// port num			

COMPORT_SETTINGS	ComPort8 	 = {
								    COM8_CONTROL_REG,
								    COM8_PRESCALE_REG,
								    COM8_STATUS_REG,
								    COM8_DATA_REG,
									CBLKP(8),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM8",					// port name
								    8};						// port num			

COMPORT_SETTINGS	ComPort9 	 = {
								    COM9_CONTROL_REG,
								    COM9_PRESCALE_REG,
								    COM9_STATUS_REG,
								    COM9_DATA_REG,
									CBLKP(9),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM9",					// port name
								    9};						// port num			

COMPORT_SETTINGS	ComPort10	 = {
								    COM10_CONTROL_REG,
								    COM10_PRESCALE_REG,
								    COM10_STATUS_REG,
								    COM10_DATA_REG,
									CBLKP(10),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM10",				// port name
								    10};					// port num			

COMPORT_SETTINGS	ComPort11	 = {
								    COM11_CONTROL_REG,
								    COM11_PRESCALE_REG,
								    COM11_STATUS_REG,
								    COM11_DATA_REG,
									CBLKP(11),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM11",				// port name
								    11};					// port num			

COMPORT_SETTINGS	ComPort12	 = {
								    COM12_CONTROL_REG,
								    COM12_PRESCALE_REG,
								    COM12_STATUS_REG,
								    COM12_DATA_REG,
									CBLKP(12),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM12",				// port name
								    12};					// port num			

COMPORT_SETTINGS	ComPort13	 = {
								    COM13_CONTROL_REG,
								    COM13_PRESCALE_REG,
								    COM13_STATUS_REG,
								    COM13_DATA_REG,
									CBLKP(13),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM13",				// port name
								    13};					// port num			

COMPORT_SETTINGS	ComPort14	 = {
								    COM14_CONTROL_REG,
								    COM14_PRESCALE_REG,
								    COM14_STATUS_REG,
								    COM14_DATA_REG,
									CBLKP(14),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM14",				// port name
								    14};					// port num			

COMPORT_SETTINGS	ComPort15	 = {
								    COM15_CONTROL_REG,
								    COM15_PRESCALE_REG,
								    COM15_STATUS_REG,
								    COM15_DATA_REG,
									CBLKP(15),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM15",				// port name
								    15};					// port num			

COMPORT_SETTINGS	ComPort16	 = {
								    COM16_CONTROL_REG,
								    COM16_PRESCALE_REG,
								    COM16_STATUS_REG,
								    COM16_DATA_REG,
									CBLKP(16),
									0,0,0,0,0,0,0,0,		// input & output buffers
									0,0,	    			// error_code,eventFlags
								    0,						// handle
									INTERNAL_PORT,		    // porttype
								    MODE_NORMAL,			// port mode
								    "COM16",				// port name
								    16};					// port num			
#endif
//-----------------------------------------------------------------------------
#endif

// A list of pointer to the com port settings
static COMPORT_SETTINGS *ComPortList[NUMCOMPORTS];

// Handle counter. Incremented each time a port is opened.
static int handleCounter = 1;

// comHandler event id
static int ComIntEventID = 0;

// Defined in COMHANDLER.ASM
//extern void enableComInterrupt(void);
extern void emptyRX(COMPORT_SETTINGS *pcs);
extern void fillTX(COMPORT_SETTINGS *pcs);
extern int getEvent(COMPORT_SETTINGS *pcs);
extern void setEvent(COMPORT_SETTINGS *pcs, int flags);

extern void comHandler(void);

#ifdef _IM3000
void InitPortPins(COMPORT_SETTINGS* pcs)
{
	switch (pcs->port) {
    	case 1:
			//Configure input and output bits
	        //Port J (3:0)
			AndCJ(0xF5);	//set port input bits
			OrCJ(0x05);		//set port output bits

			//Enable uart1_irq in IRQ1_SRC (port L)
			OrCL(0x80);		//uart1_irq
	    break;
    	case 2:
			//Configure input and output bits
	        //Port E (3:0)
			AndCE(0xF5);	//set port input bits
			OrCE(0x05);		//set port output bits

			//Enable uart2_irq in IRQ1_SRC (port L)
			OrCL(0x40);		//uart2_irq
	    break;
    	case 3:
			//Configure input and output bits
	        //Port E (7:4)
			AndCE(0x5F);	//set port input bits
			OrCE(0x50);		//set port output bits

			//Enable uart3_irq in IRQ1_SRC (port L)
			OrCL(0x20);		//uart3_irq
	    break;
	    default:
	    break;
    }
}	

//Enable the selected COM port in CRB6, making it leave reset mode
void EnComCRB(COMPORT_SETTINGS* pcs)
{
	switch (pcs->port) {
    	case 1:	OrCRB(6, 0x40); break;	//Enable UART1 in CRB6
    	case 2:	OrCRB(6, 0x20); break;	//Enable UART2 in CRB6
    	case 3:	OrCRB(6, 0x10); break;	//Enable UART3 in CRB6
	    default: break;
    }
}

//Disable the selected COM port in CRB6, making it enter reset mode
void DisComCRB(COMPORT_SETTINGS* pcs)
{
	switch (pcs->port) {
    	case 1:
			AndCJ(0xF0);		//Disable all UART1 port pins
			AndCRB(6, 0xBF);	//Disable UART1 in CRB6
		break;
    	case 2:
			AndCE(0xF0);		//Disable all UART2 port pins
			AndCRB(6, 0xDF);	//Disable UART2 in CRB6
		break;	
    	case 3:
			AndCE(0x0F);		//Disable all UART3 port pins
			AndCRB(6, 0xEF);	//Disable UART3 in CRB6
		break;
	    default: break;
    }
}
#endif

#ifdef SNAP
void InitPortPins(COMPORT_SETTINGS* pcs)
{
	if (pcs->port == 2) {
    	// COM2, "serial0"
		OrDE(EXP_TX0 | EXP_RX0);		// Set these to 1 to not invert TX/RX
		OrCE(EXP_TX0);   				// Configure UART TX line as output
		AndCE(~EXP_RX0);   				// Configure UART RX line as input
	}
	else if (pcs->port == 1) {
		// COM1, "serial1"
	 	OrDJ(EXP_TX1 | EXP_RX1);		// Set these to 1 to not invert TX/RX
		OrCJ(EXP_TX1);   				// Configure UART TX line as output
		AndCJ(~EXP_RX1);   				// Configure UART RX line as input
	} 
	else if (pcs->port == 3) {
		// COM3, "serial4"
		OrDE(EXP_TX4 | EXP_RX4);		// Set these to 1 to not invert TX/RX
		OrCE(EXP_TX4);   				// Configure UART TX line as output
		AndCE(~EXP_RX4);   				// Configure UART RX line as input
	}
}	
#endif

/*****************************************************************
 * installComInterrupt
 *
 * Install com interrupt handler.
 *
 ****************************************************************/ 
/*
void installComInterrupt()
{
	josEventAttr_t attr;

	if (ComIntEventID)
    	return;

	attr.object.name = (josName_t)"ComInt";
    attr.object.type = R_OBJECT_EVENT | R_EVENT_INTERRUPT;
    attr.object.flags = 0;
    attr.object.keyword = 0;
#ifdef _IM3000
    attr.priority = EVENTLEVEL_UART;
#else
    attr.priority = EVENTLEVEL_MIRQ1;
#endif
    attr.entry = &comHandler;
	josEventInstall(&ComIntEventID, &attr);

#ifndef _IM3000
	OrPMXC(IMR_MIRQ1_EN);				// Enable the MIRQ1 interrupt
#endif
}
*/

void installComInterrupt()
{

	if (ComIntEventID)
    	return;

	sysSetEventVector (EVENTLEVEL_UART, &comHandler);
	asm("setbit eir",  EVENTLEVEL_UART);

}


void EnablePort(COMPORT_SETTINGS *pcs, int mode)
{
	AndB(pcs->control_reg, ~UCR_MODE);
	OrB(pcs->control_reg, mode);
}

inline int comGetFlowFlagsMode(COMPORT_SETTINGS *pcs)
{
	switch (pcs->cblkp->flow_flags & FLOW_MODE) {
		case FLOW_RTSCTS:	return(FLOWCONTROL_RTSCTS);
		case FLOW_XONXOFF:	return(FLOWCONTROL_XONXOFF);
		case FLOW_ASYMM:	return(FLOWCONTROL_ASYMM);
	}
	return(FLOWCONTROL_NONE);
}

void setFlowFlagsMode(COMPORT_SETTINGS *pcs, int mode)
{
	pcs->cblkp->flow_flags &= ~FLOW_MODE;
	switch (mode) {
		case FLOWCONTROL_RTSCTS:	pcs->cblkp->flow_flags |= FLOW_RTSCTS; break;
		case FLOWCONTROL_XONXOFF:	pcs->cblkp->flow_flags |= FLOW_XONXOFF; break;
		case FLOWCONTROL_ASYMM:		pcs->cblkp->flow_flags |= FLOW_ASYMM; break;
    }
}

/*****************************************************************
 * findPort
 *
 * Examines if a com port exists.
 *
 * Parameters:	*pcs		The ports settings struct.
 *
 * Returns:		E_COM_SUCCESS if the port is found. Otherwise an 
 *				error code is returned.
 *
 ****************************************************************/ 
static int findPort(COMPORT_SETTINGS *pcs)
{
#ifdef _IM3000
	return E_COM_SUCCESS;	//So far only internal ports allowed on IM3000
#else
	unsigned char data;
    unsigned char old;

	// Some internal ports are not implemented for SNAP.
	if (pcs->control_reg == 0)
    	return E_COM_PORT_NOT_FOUND;

	if (pcs->porttype == INTERNAL_PORT)	{	// Internal port
		// Save current register value
		old = InB(pcs->control_reg);		

		// Set bit and check if really got set
		OutB(pcs->control_reg, old | 0x08);	    		
		data = InB(pcs->control_reg);
	    if (!(data & 0x08)) {
			OutB(pcs->control_reg, old);
    		return E_COM_PORT_NOT_FOUND;
		}
    		
		// Reset bit and check if really got reset
		data = InB(pcs->control_reg) & 0xf7;
		OutB(pcs->control_reg, data);
		data = InB(pcs->control_reg);
    	if (data & 0x08) {
			OutB(pcs->control_reg, old);
	    	return E_COM_PORT_NOT_FOUND;
		}

		// Restore register
		OutB(pcs->control_reg, old);		
    }
    else {	// External port
#ifdef SNAP
		// Write to and then read from scratch register
		WrPort(pcs->control_reg + UART_SPR, 0x55);		
		data = RdPort(pcs->control_reg + UART_SPR);	    

		// Check result
		return (data == 0x55) ? E_COM_SUCCESS : E_COM_PORT_NOT_FOUND;
#else
    	return E_COM_PORT_NOT_FOUND;
#endif
	}

	return E_COM_SUCCESS;
#endif
}

//////////////////////////////////////////////////////////////////
// validateBaudrate
//
// Validates baudrate value
//
//////////////////////////////////////////////////////////////////
static int validateBaudrate(COMPORT_SETTINGS* pcs, int baudrate)
{
	if (pcs->porttype == INTERNAL_PORT) {
		switch(baudrate) {
    		case 300:		return prescale_int[PRESCALE_300];	 		
    		case 600: 		return prescale_int[PRESCALE_600];
    		case 1200: 		return prescale_int[PRESCALE_1200];
    		case 2400: 		return prescale_int[PRESCALE_2400];
    		case 4800: 		return prescale_int[PRESCALE_4800];
    		case 7200: 		return prescale_int[PRESCALE_7200];
    		case 9600: 		return prescale_int[PRESCALE_9600];
    		case 14400: 	return prescale_int[PRESCALE_14400];
		    case 19200:		return prescale_int[PRESCALE_19200];
			case 28800:		return prescale_int[PRESCALE_28800];
			case 38400:		return prescale_int[PRESCALE_38400];
	    	case 57600:		return prescale_int[PRESCALE_57600];
		    case 115200:	return prescale_int[PRESCALE_115200];
		    case 230400:	return prescale_int[PRESCALE_230400];
		    case 460800:	return prescale_int[PRESCALE_460800];
		    case 921600:	return prescale_int[PRESCALE_921600];
	    }
    }
#ifdef SNAP
    else {
		switch(baudrate) {
    		case 300:		return prescale_ext[PRESCALE_300]; 		
    		case 600: 		return prescale_ext[PRESCALE_600];
    		case 1200: 		return prescale_ext[PRESCALE_1200];
    		case 2400: 		return prescale_ext[PRESCALE_2400];
    		case 4800: 		return prescale_ext[PRESCALE_4800];
    		case 7200: 		return prescale_ext[PRESCALE_7200];
    		case 9600: 		return prescale_ext[PRESCALE_9600];
    		case 14400: 	return prescale_ext[PRESCALE_14400];
		    case 19200:		return prescale_ext[PRESCALE_19200];
			case 28800:		return prescale_ext[PRESCALE_28800];
			case 38400:		return prescale_ext[PRESCALE_38400];
	    	case 57600:		return prescale_ext[PRESCALE_57600];
		    case 115200:	return prescale_ext[PRESCALE_115200];
		}
    }
#endif
	return -1;
}

static int validateDatabits(COMPORT_SETTINGS* pcs, int databits)
{
	if (pcs->porttype == INTERNAL_PORT) {
		if ((databits < DATABITS_7) || (databits > DATABITS_8))	
			return -1;
    }
    else {
		if ((databits < DATABITS_5) || (databits > DATABITS_8))	
			return -1;
    }
    return 0;
}

//////////////////////////////////////////////////////////////////
// setBaudrate
//
// Sets baudrate for a port
//
// Parameters:	control_reg		The ports control register address.
//				baudrate		Desired baudrate
//
// Returns:		
//
//////////////////////////////////////////////////////////////////
static int setBaudrate(COMPORT_SETTINGS *pcs, int baudrate)
{
	short data;	

	if ((data = validateBaudrate(pcs, baudrate)) < 0) {
		TRACE0("[setBaudrate] Invalide baudrate\n");
		return E_COM_INVALID_PARAM;
	}

	if (pcs->porttype == INTERNAL_PORT)	{	// Internal port
		OutB(pcs->prescale_reg, (InB(pcs->prescale_reg) & 0xf0) | data);
    }
    else {	// External port
#ifdef SNAP	
		WrPort(pcs->control_reg + UART_LCR, RdPort(pcs->control_reg + UART_LCR) | LCR_DLAB);	// Enable writing to divisor latch registers
		WrPort(pcs->control_reg + UART_DLM, (data & 0xff00) >> 8);	
		WrPort(pcs->control_reg + UART_DLL, data & 0xff);
		WrPort(pcs->control_reg + UART_LCR, RdPort(pcs->control_reg + UART_LCR) & ~LCR_DLAB);	// Disable divisor latch registers

		TRACE3("[setBaudrate] adr=%X, baudrate=%d, divisor=%d\n", pcs->control_reg, baudrate, data);
#else
		return E_COM_INVALID_PARAM;
#endif
    }

	return E_COM_SUCCESS;
}

//////////////////////////////////////////////////////////////////
// setCtrlReg
//
// Sets the control parameters for a port
//
// Parameters:	pcom		Pointer to a COMPORT_SETTINGS struct
//			
//
// Returns:		
//
//////////////////////////////////////////////////////////////////
static void setCtrlReg(COMPORT_SETTINGS *pcs)
{
	unsigned char settings = 0;

	if (pcs->porttype == INTERNAL_PORT)	{
    	// Internal port
		if (pcs->databits == DATABITS_7) 			settings |= UCR_DATABITS_7;
    	else if (pcs->databits == DATABITS_8) 		settings |= UCR_DATABITS_8;

		if (pcs->parity == PARITY_NONE)				settings |= UCR_PARITY_NONE;
    	else if (pcs->parity == PARITY_ODD)			settings |= UCR_PARITY_ODD;
		else if (pcs->parity == PARITY_EVEN)		settings |= UCR_PARITY_EVEN;
	
		if (pcs->stopbits == STOPBITS_1)			settings |= UCR_STOPBITS_1;
		else if (pcs->stopbits == STOPBITS_2)		settings |= UCR_STOPBITS_2;

		// Set parity, stop bits and data bits in UCR, without
	    // touching mode bits or RTS bit
		OutB(pcs->control_reg, (InB(pcs->control_reg)  & (UCR_MODE | UCR_RTS)) | settings);
    }
#ifdef SNAP
    else {
    	// External port
		if (pcs->databits == DATABITS_5) 			settings |= LCR_5BIT;
		else if (pcs->databits == DATABITS_6) 		settings |= LCR_6BIT;
		else if (pcs->databits == DATABITS_7) 		settings |= LCR_7BIT;
    	else if (pcs->databits == DATABITS_8) 		settings |= LCR_8BIT;

		if (pcs->parity == PARITY_NONE)				settings |= LCR_NOPAR;
    	else if (pcs->parity == PARITY_ODD)			settings |= LCR_ODDPAR;
		else if (pcs->parity == PARITY_EVEN)		settings |= LCR_EVENPAR;
	
		if (pcs->stopbits == STOPBITS_1)			settings |= LCR_1STOP;
		else if (pcs->stopbits == STOPBITS_2)		settings |= LCR_2STOP;
	
		// Set parity, stop bits and data bits in LCR
		WrPort(pcs->control_reg + UART_LCR, settings);
    }
#endif

	TRACE2("[setCtrlReg] reg=0x%X, settings=0x%02X\n", pcs->control_reg, settings);
}

//////////////////////////////////////////////////////////////////
// GetBigInbufDataLength
//
// Returns the number of bytes in the big input buffer.
//
// Parameters:	pcs		com port
//
// Returns:		number of bytes in the big input buffer.
//
//////////////////////////////////////////////////////////////////
inline int GetBigInbufDataLength(COMPORT_SETTINGS* pcs)
{
	int	diff;

	if ((diff = pcs->inbuf_inptr - pcs->inbuf_outptr) < 0)
    	diff += (pcs->inbuf_endptr - pcs->inbuf_bufptr);
	
	return diff;
}

inline int GetBigInbufSpace(COMPORT_SETTINGS* pcs)
{
	int	diff;

	if ((diff = pcs->inbuf_outptr - pcs->inbuf_inptr - 1) < 0)
    	diff += (pcs->inbuf_endptr - pcs->inbuf_bufptr);
	
	return diff;
}

//////////////////////////////////////////////////////////////////
// GetSmallInbufDataLength
// Returns the number of bytes in the small input buffer.
// This routine is always called with interrupts ENABLED!
//
// Parameters:	pcs		com port
//
// Returns:		number of bytes in the small input buffer.
//
//////////////////////////////////////////////////////////////////
inline int GetSmallInbufDataLength(COMPORT_SETTINGS* pcs)
{
	Dis();
	int	ret = (pcs->cblkp->inbuf_inptr - pcs->cblkp->inbuf_outptr) & 0xFF;
	Enb();
	return ret;
}

//////////////////////////////////////////////////////////////////
// GetSmallInbufSpace
// Returns available space in the small input buffer.
// This routine is always called with interrupts DISABLED!
//
// Parameters:	pcs		com port
//
// Returns:		available space in the small input buffer.
//
//////////////////////////////////////////////////////////////////
inline int GetSmallInbufSpace(COMPORT_SETTINGS* pcs)
{
	return (pcs->cblkp->inbuf_outptr - pcs->cblkp->inbuf_inptr - 1) & 0xFF;
}

//////////////////////////////////////////////////////////////////
// GetBigOutbufSpace
//
// Returns available space in the big output buffer.
//
// Parameters:	pcs		com port
//
// Returns:		available space in the big output buffer.
//
//////////////////////////////////////////////////////////////////
inline int GetBigOutbufSpace(COMPORT_SETTINGS* pcs)
{
	int	diff;

	if ((diff = pcs->outbuf_outptr - pcs->outbuf_inptr - 1) < 0)
    	diff += (pcs->outbuf_endptr - pcs->outbuf_bufptr);
	
	return diff;
}

//////////////////////////////////////////////////////////////////
// GetSmallOutbufSpace
// Returns available space in the small output buffer.
// This routine is always called with interrupts ENABLED!
//
// Parameters:	pcs		com port
//
// Returns:		available space in the small output buffer.
//
//////////////////////////////////////////////////////////////////
inline int GetSmallOutbufSpace(COMPORT_SETTINGS* pcs)
{
	Dis();
	int	ret = (pcs->cblkp->outbuf_outptr - pcs->cblkp->outbuf_inptr - 1) & 0xFF;
	Enb();
	return ret;
}

//////////////////////////////////////////////////////////////////
// GetSmallOutbufDataLength
// Returns the number of bytes in the small output buffer.
// This routine is always called with interrupts DISABLED!
//
// Parameters:	pcs		com port
//
// Returns:		number of bytes in the small output buffer.
//
//////////////////////////////////////////////////////////////////
inline int GetSmallOutbufDataLength(COMPORT_SETTINGS* pcs)
{
	return (pcs->cblkp->outbuf_inptr - pcs->cblkp->outbuf_outptr) & 0xFF;
}

//////////////////////////////////////////////////////////////////
// getComPortFromHandle
//
//
//////////////////////////////////////////////////////////////////
COMPORT_SETTINGS* getComPortFromHandle(int handle)
{
	COMPORT_SETTINGS	*pcs;

    pcs = ComPortList[(handle-1) & HANDLEMASK];
    if (pcs->handle != handle)
    	return NULL;
	
	return pcs;
}

//////////////////////////////////////////////////////////////////
// comGetNumPorts
//
// Returns the maximal number of serial ports supported by the driver.
//
// Returns:		Max number of supported ports.
//
//////////////////////////////////////////////////////////////////
int comGetNumPorts()
{
	return LAST_PORT;
}

//////////////////////////////////////////////////////////////////
// comGetPortName
//
// Returns the name of the specified port.
//
// Returns:		Max number of supported ports.
//
//////////////////////////////////////////////////////////////////
const char *comGetPortName(int port)
{
	// Check port
    if ((port < FIRST_PORT) || (port > LAST_PORT)) 
    	return NULL;

	return(ComPortList[port-1]->name);
}

//////////////////////////////////////////////////////////////////
// comGetEventFlags
//
// Returns the eventFlags of the specified port. Also clears the
// event flags after reading them.
//
// Returns:		eventFlags
//
//////////////////////////////////////////////////////////////////
int comGetEventFlags(int handle)
{
	COMPORT_SETTINGS	*pcs;

    pcs = ComPortList[(handle-1) & HANDLEMASK];
    if (pcs->handle != handle)
    	return E_COM_INVALID_PARAM;

	return getEvent(pcs);
}

//////////////////////////////////////////////////////////////////
// comSetEventFlags
//
// Sets the eventFlags of the specified port.
//
// Returns:		nothing
//
//////////////////////////////////////////////////////////////////
void comSetEventFlags(int handle, int flags)
{
	COMPORT_SETTINGS	*pcs;

    pcs = ComPortList[(handle-1) & HANDLEMASK];
    if (pcs->handle != handle)
    	return;

	setEvent(pcs,flags);
}

//////////////////////////////////////////////////////////////////
// comForceClose
//
// Forces a close on a port. No handle needed, just the port number
//
// Parameters:	port		The port number
//				
//
// Returns:		E_COM_SUCCESS or E_COM_INVALID_PARAM			
//
//////////////////////////////////////////////////////////////////
int comForceClose(int port)
{
	COMPORT_SETTINGS	*pcs;		

	// Check port
    if ((port < FIRST_PORT) || (port > LAST_PORT)) 
    	return E_COM_INVALID_PARAM;

   	pcs = ComPortList[port-1];
    if (pcs->handle == 0)
    	return E_COM_INVALID_PARAM;

	// Flush port before closing it
	comFlush(pcs->handle);

	if (pcs->porttype == INTERNAL_PORT) {
		// Disable all interrupts and disable port
		AndB(pcs->prescale_reg, ~UIR_ALL);
		OutB(pcs->control_reg, 0);
    }
#ifdef SNAP
    else {	// External port
		// Disable interrupts
		WrPort(pcs->control_reg + UART_IER, 0);		
    }
#endif

	free(pcs->inbuf_bufptr);
	free(pcs->outbuf_bufptr);
	memset(&(pcs->inbuf_bufptr),0,32); // Clear ptr area
    pcs->handle = 0;

	return E_COM_SUCCESS;																 
}

//////////////////////////////////////////////////////////////////
// comSetReceiveTimeout
//
// Sets the receive timeout period.
//
// Parameters:	handle		Handle to the port
//				millis		Number of milliseconds to wait for data.
//				
// Returns:		E_COM_SUCCESS if the timeout was successfully set,
//				otherwise an error code is returned.
//
//////////////////////////////////////////////////////////////////
int comSetReceiveTimeout(int handle, int millis)
{
	COMPORT_SETTINGS	*pcs;

	// Check the handle and millis
    pcs = ComPortList[(handle-1) & HANDLEMASK];
    if ((pcs->handle != handle) || (millis < 0))
    	return E_COM_INVALID_PARAM;

	pcs->rcvTimeout = millis;

	return E_COM_SUCCESS;
}

//////////////////////////////////////////////////////////////////
// comGetReceiveTimeout
//
// Gets the current receive timeout period.
//
// Parameters:	handle		Handle to the port
//				
// Returns:		The currently set receive timeout period or,
//			    if an error ocurs or the timeout is not previously
//			    set, E_COM_INVALID_PARAM.
//
//////////////////////////////////////////////////////////////////
int comGetReceiveTimeout(int handle)
{
	COMPORT_SETTINGS	*pcs;

	// Check the handle
    pcs = ComPortList[(handle-1) & HANDLEMASK];
    if (pcs->handle != handle)
    	return E_COM_INVALID_PARAM;

    if (pcs->rcvTimeout <= 0)
    	return E_COM_INVALID_PARAM;

    return pcs->rcvTimeout;
}

//////////////////////////////////////////////////////////////////
// comSetReceiveThreshold
//
// Enables receive threshold.
//
// Parameters:	handle		Handle to the port
//				thresh		Threshold level
//				
// Returns:		
//
//////////////////////////////////////////////////////////////////
int comSetReceiveThreshold(int handle, int thresh)	
{
	COMPORT_SETTINGS	*pcs;

	// Check the handle and millis
    pcs = ComPortList[(handle-1) & HANDLEMASK];
    if (pcs->handle != handle)
    	return E_COM_INVALID_PARAM;

	if (thresh >= 0)
		pcs->rcvThreshold = thresh;
    else if (thresh == -1)
		pcs->rcvThreshold = -1;
    else
    	return E_COM_INVALID_PARAM;

	return E_COM_SUCCESS;
}

//////////////////////////////////////////////////////////////////
// comGetReceiveThreshold
//
// Gets the integer value of the receive threshold.
//
// Parameters:	handle		Handle to the port
//				
// Returns:		Number of bytes for receive threshold, or if
//			    error occurs or the threshold is not previously
//			    set, E_COM_INVALID_PARAM.
//
//////////////////////////////////////////////////////////////////
int comGetReceiveThreshold(int handle)	
{
	COMPORT_SETTINGS	*pcs;

	// Check the handle
    pcs = ComPortList[(handle-1) & HANDLEMASK];
    if (pcs->handle != handle)
    	return E_COM_INVALID_PARAM;

    if (pcs->rcvThreshold < 0)
    	return E_COM_INVALID_PARAM;

    return pcs->rcvThreshold;
}

#pragma warn variables off
/*******************************************************************************
* @func			comInit
* @shortdesc 	Initializes the COM driver.
* @desc			Initializes the COM driver.
*				Must be called once before using any other functions of the COM driver.
* @return		int Always returns E_COM_SUCCESS. 
* @see			comOpen
*	
*******************************************************************************/
int comInit()
{
#ifdef _IM3000
	ComPortList[0]  = &ComPort1;
	ComPortList[1]  = &ComPort2;
	ComPortList[2]  = &ComPort3;

#else	// IM1000

#ifdef SNAP
	// Order by "serial0", "serial1", ...
	ComPortList[0]  = &ComPort2;	
	ComPortList[1]  = &ComPort1;
#ifndef _BOOTLOADER
	ComPortList[2]  = &ComPort4;
	ComPortList[3]  = &ComPort5;
	ComPortList[4]  = &ComPort3;
	ComPortList[5]  = &ComPort6;
	ComPortList[6]  = &ComPort7;
	ComPortList[7]  = &ComPort8;
	ComPortList[8]  = &ComPort9;
	ComPortList[9]  = &ComPort10;
	ComPortList[10] = &ComPort11;
	ComPortList[11] = &ComPort12;
	ComPortList[12] = &ComPort13;
	ComPortList[13] = &ComPort14;
	ComPortList[14] = &ComPort15;
	ComPortList[15] = &ComPort16;
#endif  // _BOOTLOADER
#else
	ComPortList[0]  = &ComPort1;
	ComPortList[1]  = &ComPort2;
	ComPortList[2]  = &ComPort3;
	ComPortList[3]  = &ComPort4;
	ComPortList[4]  = &ComPort5;
	ComPortList[5]  = &ComPort6;
	ComPortList[6]  = &ComPort7;
	ComPortList[7]  = &ComPort8;
	ComPortList[8]  = &ComPort9;
	ComPortList[9]  = &ComPort10;
	ComPortList[10] = &ComPort11;
	ComPortList[11] = &ComPort12;
	ComPortList[12] = &ComPort13;
	ComPortList[13] = &ComPort14;
	ComPortList[14] = &ComPort15;
	ComPortList[15] = &ComPort16;
#endif
#endif

	// Enable COM interrupt and install interrupt vector
	installComInterrupt();							

	return E_COM_SUCCESS;
}
#pragma warn variables on

/*******************************************************************************
* @func			comPortExists
* @shortdesc	Tests if port exists.
* @desc			Tests if a COM port exists.
* @param		int port Port number of port to check.
*				Must be between 1 and the number of available ports, inclusive.
* @return		int Non-zero if the port exists, otherwise 0 is returned. 
*******************************************************************************/
int comPortExists(int port)
{
	COMPORT_SETTINGS	*pcs;

	// Check port
    if ((port < FIRST_PORT) || (port > LAST_PORT)) 
    	return 0;

   	pcs = ComPortList[port-1];
	return(findPort(pcs) == E_COM_SUCCESS ? 1 : 0);
}

/*******************************************************************************
* @func	comOpen
*
* @shortdesc	Opens a port.
*
* @desc			Opens a COM port.
*
* @param		int port Port number of port to open.
*				Must be between 1 and the number of available ports, inclusive.
* @param		int baudrate Baud rate setting for the port.
* @param		int databits Number of data bits setting for the port.
*				Use one of the constants DATABITS_5 .. DATABITS_8.
* @param		int stopbits Number of stop bits setting for the port.
*				Use one of the constants STOPBITS_1 or STOPBITS_2.
* @param		int parity Parity setting for the port.
*				Use one of the constants PARITY_NONE, PARITY_ODD, or PARITY_EVEN.
* @param		int flowcontrol Flow control setting for the port.
*				Use one of the constants FLOWCONTROL_NONE, FLOWCONTROL_RTSCTS,
*				FLOWCONTROL_XONXOFF or FLOWCONTROL_ASYMM.
* @param		int access Access setting for the port.
*				Use one of the constants COM_READ, COM_WRITE, or COM_READWRITE.
* @param		int bufsize Buffer sizes for the port.
*				Both input and output buffers will be set to this size. Size will be
*				rounded upwards to a multiple of 256.
*
* @return		int Port handle if successful, otherwise negative error code. 
*
* @see			comInit
* @see			comClose
*	
* @ex
*	#include &lt;stdio.h&gt;
*	#include &lt;string.h&gt;
*	#include "com.h"
*		 
*	int		com1;
*	char	buf[80];
*
*	void main(void)
*	{
*		comInit();
*
*		com1 = comOpen(1, 115200, DATABITS_8, STOPBITS_1, PARITY_NONE, FLOWCONTROL_RTSCTS, COM_READWRITE, 512);
*	    				
*		sprintf(buf,"Hello world!\r\n");
*		comWrite(com1,(unsigned char*)buf,strlen(buf));
*
*		comClose(com1);
*	}
*
*******************************************************************************/
#pragma warn variables off
int comOpen(int port, int baudrate, int databits, int stopbits, int parity, 
			int flowcontrol, int access, int bufsize)
{
	COMPORT_SETTINGS	*pcs;

	// Check port
    if ((port < FIRST_PORT) || (port > LAST_PORT)) 
    	return E_COM_INVALID_PARAM;

	// Check if the port exists
   	pcs = ComPortList[port-1];
    if (findPort(pcs))
    	return E_COM_PORT_NOT_FOUND;

	// Check if the port is already open
	if (pcs->handle!=0)     	
	    return E_COM_PORT_ALREADY_OPEN;			

	// Check parameters
	if (access!=COM_READ && access!=COM_WRITE && access!=COM_READWRITE)
		return E_COM_INVALID_PARAM;

	if (validateBaudrate(pcs, baudrate) < 0)
		return E_COM_INVALID_PARAM;

	if (validateDatabits(pcs, databits) < 0)
		return E_COM_INVALID_PARAM;

	if (parity != PARITY_NONE && parity != PARITY_ODD && parity != PARITY_EVEN)
		return E_COM_INVALID_PARAM;

	if (stopbits != STOPBITS_1 && stopbits != STOPBITS_2)
		return E_COM_INVALID_PARAM;

	if (flowcontrol != FLOWCONTROL_NONE && flowcontrol != FLOWCONTROL_RTSCTS &&
		flowcontrol != FLOWCONTROL_XONXOFF && flowcontrol != FLOWCONTROL_ASYMM)
		return E_COM_INVALID_PARAM;

#ifdef SNAP
	// On SNAP, the hardware handshake signals are shared by the internal ports.
	// Only one of them can be the owner of the signals which are claimed by 
	// the comSetRTSCTSFlowControlEnable() function.
    if (((flowcontrol == FLOWCONTROL_RTSCTS) || (flowcontrol == FLOWCONTROL_ASYMM)) &&
		(pcs->porttype == INTERNAL_PORT))
	{
    	if (hwhandshakeOwner != pcs->port)
			return E_COM_INVALID_PARAM;
    }
#endif
	
	if (bufsize < MINBUFSIZE)			// Don't allow smaller buffers than MINBUFSIZE
    	bufsize = MINBUFSIZE;
	bufsize = (bufsize+255)&0xFFFFFF00;	// Make bufsize a multiple of 256

	// Parameters were ok, put them in settings block
    pcs->access = access;
	pcs->baudrate = baudrate;	
	pcs->databits = databits;
	pcs->parity = parity;
	pcs->stopbits = stopbits;
	pcs->cblkp->flow_flags = 0;
	setFlowFlagsMode(pcs,flowcontrol);

	//Init other stuff in settings block
	pcs->event_flags = 0;
    pcs->rcvTimeout = -1;		// Timeout function disabled
    pcs->rcvFramingByte = -1;   // Framing disabled
    pcs->rcvThreshold = -1;   	// Threshold disabled

	// Init buffers
    if (!(pcs->inbuf_bufptr = (unsigned char*)malloc(bufsize)))
    	return E_COM_OUT_OF_MEMORY;
    pcs->inbuf_inptr = pcs->inbuf_bufptr;
    pcs->inbuf_outptr = pcs->inbuf_bufptr;
    pcs->inbuf_endptr = pcs->inbuf_bufptr + bufsize;

    if (!(pcs->outbuf_bufptr = (unsigned char*)malloc(bufsize)))
    	return E_COM_OUT_OF_MEMORY;
    pcs->outbuf_inptr = pcs->outbuf_bufptr;
    pcs->outbuf_outptr = pcs->outbuf_bufptr;
    pcs->outbuf_endptr = pcs->outbuf_bufptr + bufsize;

	// Init small buffer pointers
	*(int*)pcs->cblkp = 0;

#ifdef _IM3000
	EnComCRB(pcs);	//Enable the selected port so we can access it
#endif

	// Setup port
	setBaudrate(pcs, pcs->baudrate);					
	setCtrlReg(pcs);									
	
	if (pcs->porttype == INTERNAL_PORT) {	// Internal port
    	unsigned char apa;

//		if ((pcs->cblkp->flow_flags & FLOW_MODE) == FLOW_ASYMM)
   			SET_RTS()									// Activate RTS (flow enabled)
//		else
// 			RESET_RTS()									// Deactivate RTS (flow disabled)
	    EnablePort(pcs, UCR_DISABLED);				// Disable port
		InitPortPins(pcs);							// Set up port pin directions

	    AndB(pcs->prescale_reg, ~UIR_ALL);		// Disable all interrupts
	    apa = InB(pcs->data_reg);						// Empty receive register
	    OutB(pcs->status_reg, 0);					// Clear error flags

		if (pcs->access & COM_READ)						//If read access enable RX and error interrupts 
		    OrB(pcs->prescale_reg, UIR_RX | UIR_ERROR);

		if (((pcs->cblkp->flow_flags & FLOW_MODE) == FLOW_RTSCTS) ||
			((pcs->cblkp->flow_flags & FLOW_MODE) == FLOW_ASYMM))
		{
			if (pcs->access & COM_WRITE) {				//If write access and RTS/CTS enable status interrupt
				READ_CTS(apa);
			    if (!apa)								//Set flow_out_off flag if RTS/CTS used and CTS not set
			    	pcs->cblkp->flow_flags |= FLOW_OUT_OFF;
		    	OrB(pcs->prescale_reg, UIR_STATUS);
		    }
//			if ((pcs->cblkp->flow_flags & FLOW_MODE) == FLOW_RTSCTS)
//   				SET_RTS()								//Activate RTS (flow enabled) if RTS/CTS used
	    }

		pcs->handle = (handleCounter++ << 8) | port;	// Create handle from port number and counter
		pcs->error_code = E_COM_SUCCESS;

		if (pcs->mode == MODE_IRDA)
		    EnablePort(pcs, UCR_IRDA);					// Enable port in IrDA mode
		else
		    EnablePort(pcs, UCR_NORMAL);				// Enable port
    }
#ifdef SNAP
    else {									// External serial port
    	unsigned char apa;

//		if ((pcs->cblkp->flow_flags & FLOW_MODE) == FLOW_ASYMM)
			WrPort(pcs->control_reg + UART_MCR, RdPort(pcs->control_reg + UART_MCR) | MCR_RTS);	//Activate RTS (flow enabled) if RTS/CTS used
//		else
//			WrPort(pcs->control_reg + UART_MCR, RdPort(pcs->control_reg + UART_MCR) & ~MCR_RTS);	//Deactivate RTS (flow enabled) if RTS/CTS used

		WrPort(pcs->control_reg + UART_IER, 0);		    // Disable all interrupts
	    WrPort(pcs->control_reg + UART_FCR, 0x01);		// Enable FIFO
	    WrPort(pcs->control_reg + UART_FCR, 0x07);		// Clear FIFOs and set trigger level 1
		if ((RdPort(pcs->control_reg + UART_ISR) & 0x40) == 0)
	    	// No FIFO available. Disable FIFO mode
		    WrPort(pcs->control_reg + UART_FCR, 0);
	    apa = RdPort(pcs->control_reg + UART_RHR);		// Empty receive register
	    apa = RdPort(pcs->control_reg + UART_LSR);		// Clear error flags

		pcs->handle = (handleCounter++ << 8) | port;	// Create handle from port number and counter
		pcs->error_code = E_COM_SUCCESS;

	   	extSerialPortOpen(pcs); 						// Open the external port

		if (pcs->access & COM_READ)						//If read access enable RBF and LS interrupts 
			WrPort(pcs->control_reg + UART_IER, RdPort(pcs->control_reg + UART_IER) | IER_RBF | IER_LS);		

		if (((pcs->cblkp->flow_flags & FLOW_MODE) == FLOW_RTSCTS) ||
			((pcs->cblkp->flow_flags & FLOW_MODE) == FLOW_ASYMM))
		{
			if (pcs->access & COM_WRITE) {				//If write access and RTS/CTS enable MS interrupt
			    if (!(RdPort(pcs->control_reg + UART_MSR) | MSR_CTS))	//Set flow_out_off flag if RTS/CTS used and CTS not set
			    	pcs->cblkp->flow_flags |= FLOW_OUT_OFF;
				WrPort(pcs->control_reg + UART_IER, RdPort(pcs->control_reg + UART_IER) | IER_MS);		
		    }
//			if ((pcs->cblkp->flow_flags & FLOW_MODE) == FLOW_RTSCTS)
//				WrPort(pcs->control_reg + UART_MCR, RdPort(pcs->control_reg + UART_MCR) | MCR_RTS);	//Activate RTS (flow enabled) if RTS/CTS used
		}
    }
#endif

	return pcs->handle;
}
#pragma warn variables on

/*******************************************************************************
* @func			comClose
* @shortdesc    Closes port.
* @desc			Closes a port that has previously been opened by <var>comOpen</var>. 
*				The port is flushed before it is closed.
* @param		int handle Port handle returned by <var>comOpen</var>.
* @return		int E_COM_SUCCESS if successful, otherwise negative error code. 
* @see			comOpen
* @see			comFlush
*******************************************************************************/
int comClose(int handle)
{
	COMPORT_SETTINGS	*pcs;		
	
	// Check the handle
    pcs = ComPortList[(handle-1) & HANDLEMASK];
    if (pcs->handle != handle)
    	return E_COM_INVALID_PARAM;

	// Flush port before closing it
	comFlush(handle);

	if (pcs->porttype == INTERNAL_PORT) {
		// Disable all interrupts and disable port
	    AndB(pcs->prescale_reg, ~UIR_ALL);
   		OutB(pcs->control_reg, 0);	    		
#ifdef _IM3000
		DisComCRB(pcs);
#endif
    }
#ifdef SNAP
    else {	// External port
		// Disable all interrupts
		WrPort(pcs->control_reg + UART_IER, 0);		
	    // Close the external port
	   	extSerialPortClose(pcs); 				
    }
#endif

	free(pcs->inbuf_bufptr);
	free(pcs->outbuf_bufptr);
	memset(&(pcs->inbuf_bufptr),0,32); // Clear ptr area
    pcs->handle = 0;

	return E_COM_SUCCESS;																 
}

/*******************************************************************************
* @func		comRead
*
* @shortdesc Read data.
* @desc		Reads a number of bytes from the input buffer of the port. Less data
*			than specified can be read if framing is enabled or if less is
*			available.
*
* @param	int handle Port handle returned by <var>comOpen</var>.
* @param	unsigned char* buffer Pointer to buffer to place the read data in.
* @param	int length Number of bytes to read.
*
* @return	int If successful, number of read bytes. Otherwise, negative error
*			code. 
*	
* @see		comOpen
* @see		comWrite
*	
* @see		comSetReceiveFraming
*******************************************************************************/
int comRead(int handle, unsigned char* buffer, int length)
{
	int buflen;				// Number of bytes in the buffer
    int rest;
    unsigned char* p;
    COMPORT_SETTINGS* pcs;

	// Check the handle
    pcs = ComPortList[(handle-1) & HANDLEMASK];
    if (pcs->handle != handle)
    	return E_COM_INVALID_PARAM;

	// Check error code
    if (pcs->error_code != E_COM_SUCCESS) {
    	// Error reported
	    int error = pcs->error_code;
	    // Clear error
	    pcs->error_code = E_COM_SUCCESS;
	    return error;
    }

	//Get number of bytes in big buffer, if not enough then check small buffer
	if (length > (buflen = GetBigInbufDataLength(pcs))) {
		//If any bytes in small buffer then empty them into big buffer
		if (GetSmallInbufDataLength(pcs)) {
			emptyRX(pcs);
			//Get number of bytes in big buffer again, return if empty 
			if (!(buflen = GetBigInbufDataLength(pcs)))
    			return 0;
	    }
		//Make sure we don't read more than available
		if (length > buflen)
    		length = buflen;
    }

	// Check that length is valid
	if (length <= 0)
    	return 0;

    //Calculate remaining length after buffer wrap - negative if no wrap.
	//Also adjust length if there will be wrap. 
	if ((rest = length - (pcs->inbuf_endptr - pcs->inbuf_outptr)) > 0)
		length -= rest;

	//Is framing enabled? Then scan for framing byte and adjust length if found.
   	if ((pcs->rcvFramingByte != -1) && (p = memchr(pcs->inbuf_outptr, pcs->rcvFramingByte, length))) {
		//Framing byte found, adjust length and rest (rest will be <= 0)
		rest = p - pcs->inbuf_outptr + 1 - length;
    	length += rest;
    }

	//Copy data from buffer
	memcpy(buffer, pcs->inbuf_outptr, length);
	pcs->inbuf_outptr += length;

	//Was there a buffer wrap? Then do the rest, if any.
	if (rest >= 0) {
		//Is framing enabled? Then scan for framing byte and adjust rest if found.
	   	if ((pcs->rcvFramingByte != -1) && (p = memchr(pcs->inbuf_bufptr, pcs->rcvFramingByte, rest))) { 
			//Framing byte found, adjust rest
			rest = p - pcs->inbuf_bufptr + 1;
	    }

		//Copy rest of data from buffer
	 	memcpy(buffer+length, pcs->inbuf_bufptr, rest);
		pcs->inbuf_outptr = pcs->inbuf_bufptr + rest;
    	length += rest;
    }

	//Transfer data from small input buffer to big
    //This could possibly be done conditionally, if it has not been done above
	if (GetSmallInbufDataLength(pcs))
		emptyRX(pcs);

	//If inward flow is turned off, then turn it on again
    //if there is space in the small input buffer
	if (pcs->porttype == INTERNAL_PORT) {
		Dis();
#ifdef _IM3000
		{
		    int mask;
		    // Reset IRQ1_EN for current port
		    mask = 1 << (8 - pcs->port);
			AndCL(~mask);

			if (GetSmallInbufSpace(pcs) > FLOWOFF_LIMIT*2) {
				if ((pcs->cblkp->flow_flags & (FLOW_IN_OFF | FLOW_MODE)) == (FLOW_IN_OFF | FLOW_XONXOFF)) {
					//Turn on flow again
				    pcs->cblkp->flow_flags &= ~FLOW_IN_OFF;
					//Set up XON for transmission
				    pcs->cblkp->flow_flags |= FLOW_TXXON;
				    OrB(pcs->prescale_reg, UIR_TX);
			    }
				else if ((pcs->cblkp->flow_flags & (FLOW_IN_OFF | FLOW_MODE)) == (FLOW_IN_OFF | FLOW_RTSCTS)) {
					//Turn on flow again
				    pcs->cblkp->flow_flags &= ~FLOW_IN_OFF;
					//Activate RTS (flow enabled)
	    			SET_RTS()
			    }
		    }
		    // Set IRQ1_EN for current port
			OrCL(mask);
		}
#else
		AndB(PMXC_IMR_addr,~IMR_MIRQ1_EN);
		if (GetSmallInbufSpace(pcs) > FLOWOFF_LIMIT*2) {
			if ((pcs->cblkp->flow_flags & (FLOW_IN_OFF | FLOW_MODE)) == (FLOW_IN_OFF | FLOW_XONXOFF)) {
				//Turn on flow again
			    pcs->cblkp->flow_flags &= ~FLOW_IN_OFF;
				//Set up XON for transmission
			    pcs->cblkp->flow_flags |= FLOW_TXXON;
			    OrB(pcs->prescale_reg, UIR_TX);
		    }
		    else if ((pcs->cblkp->flow_flags & (FLOW_IN_OFF | FLOW_MODE)) == (FLOW_IN_OFF | FLOW_RTSCTS)) {
				//Turn on flow again
			    pcs->cblkp->flow_flags &= ~FLOW_IN_OFF;
				//Activate RTS (flow enabled)
    			SET_RTS()
		    }
	    }
		OrB(PMXC_IMR_addr,IMR_MIRQ1_EN);
#endif
		Enb();
    }
#ifdef SNAP
    else {		
		Dis();
		if (GetSmallInbufSpace(pcs) > FLOWOFF_LIMIT*2) {
			if ((pcs->cblkp->flow_flags & (FLOW_IN_OFF | FLOW_MODE)) == (FLOW_IN_OFF | FLOW_XONXOFF)) {
				//Turn on flow again
			    pcs->cblkp->flow_flags &= ~FLOW_IN_OFF;
		    	//Set up XON for transmission
			    pcs->cblkp->flow_flags |= FLOW_TXXON;
				WrPort(pcs->control_reg + UART_IER, RdPort(pcs->control_reg + UART_IER) | IER_TBE);		
		    }
		    else if ((pcs->cblkp->flow_flags & (FLOW_IN_OFF | FLOW_MODE)) == (FLOW_IN_OFF | FLOW_RTSCTS)) {
				//Turn on flow again
			    pcs->cblkp->flow_flags &= ~FLOW_IN_OFF;
			    //Activate RTS (flow enabled)
				WrPort(pcs->control_reg + UART_MCR, RdPort(pcs->control_reg + UART_MCR) | MCR_RTS);
		    }
	    }
		Enb();
    }
#endif    

#ifdef DUMPDATA
	if (length && (pcs->port == 3)) {
    	char sz[256];
	    int i, c;
	    for (i=0,c=0; i<length && c < 254; i++) {
	    	sz[c++] = hex[buffer[i] >> 4];
	    	sz[c++] = hex[buffer[i] & 0x0f];
	    }
	    sz[c] = '\0';
	    printf("Read(%d): %s\n", length, sz);
    }
#endif

	return length;
}

/*******************************************************************************
* @func		comWrite
*
* @shortdesc Write data.
* @desc		Writes a number of bytes to the output buffer of the port. Less data
*			than specified can be written if there is room for less in the buffer.
*
* @param	int handle Port handle returned by <var>comOpen</var>.
*
* @param	unsigned char* buffer Pointer to data to write.
*
* @param	int length Number of bytes to write.
*
* @return	int If successful, number of written bytes. Otherwise, negative error
*			code. 
*	
* @see		comOpen
*	
* @see		comRead
*	
*******************************************************************************/
int comWrite(int handle, const unsigned char* buffer, int length)
{
	int buflen;				// Available space in the buffer
    int rest;
    COMPORT_SETTINGS* pcs;
    
	// Check the handle
    pcs = ComPortList[(handle-1) & HANDLEMASK];
    if (pcs->handle != handle)
    	return E_COM_INVALID_PARAM;

	// Return if port not opened with write access
	if (!(pcs->access & COM_WRITE))
    	return 0;

	//Get free space in big buffer, if not enough then check small buffer
	if (length > (buflen = GetBigOutbufSpace(pcs))) {
		//If any space in small buffer then fill it from big buffer
		if (GetSmallOutbufSpace(pcs)) {
			fillTX(pcs);
			//Get free space in big buffer again, return if full 
			if (!(buflen = GetBigOutbufSpace(pcs)))
    			return 0;
	    }
		//Make sure we don't write more than there's room for
		if (length > buflen)
    		length = buflen;
    }

	// Check that length is valid
	if (length <= 0)
    	return 0;

#ifdef DUMPDATA
	if (pcs->port == 3) {
    	char sz[256];
	    int i, c;
	    for (i=0,c=0; i<length && c < 254; i++) {
	    	sz[c++] = hex[buffer[i] >> 4];
	    	sz[c++] = hex[buffer[i] & 0x0f];
	    }
	    sz[c] = '\0';
	    printf("Write(%d): %s\n", length, sz);
    }
#endif

    //Calculate remaining length after buffer wrap - negative if no wrap.
	//Also adjust length if there will be wrap. 
	if ((rest = length - (pcs->outbuf_endptr - pcs->outbuf_inptr)) > 0)
		length -= rest;

	// Copy data to the big output buffer
	memcpy(pcs->outbuf_inptr, buffer, length);
	pcs->outbuf_inptr += length;

	//Was there a buffer wrap? Then do the rest, if any.
	if (rest >= 0) {
		//Copy rest of data to buffer
		memcpy(pcs->outbuf_bufptr, buffer+length, rest);
		pcs->outbuf_inptr  = pcs->outbuf_bufptr + rest;
		length += rest;
    }

	//Transfer data from big output buffer to small
    //This could possibly be done conditionally, if it has not been done above
	if (GetSmallOutbufSpace(pcs))
		fillTX(pcs);

	//If outward flow is not turned off, then enable TX empty
    //interrupt if there is data in the small output buffer
	if (pcs->porttype == INTERNAL_PORT) {
		Dis();
#ifdef _IM3000
		{
	    int mask;
	    // Reset IRQ1_EN for current port
	    mask = 1 << (8 - pcs->port);
		AndCL(~mask);
		if (GetSmallOutbufDataLength(pcs)) {
			if ((pcs->cblkp->flow_flags & FLOW_MODE) == FLOW_ASYMM)
				//Activate RTS if asymmetric RTS/CTS used
//	   			SET_RTS()
				RESET_RTS()
			if (!(pcs->cblkp->flow_flags & FLOW_OUT_OFF))
				// Enable TX Empty interrupt if outward flow is not turned off
			    OrB(pcs->prescale_reg, UIR_TX);
	    }
	    // Set IRQ1_EN for current port
		OrCL(mask);
	    }
#else
		AndB(PMXC_IMR_addr,~IMR_MIRQ1_EN);
		if (GetSmallOutbufDataLength(pcs)) {
			if ((pcs->cblkp->flow_flags & FLOW_MODE) == FLOW_ASYMM)
				//Activate RTS if asymmetric RTS/CTS used
				SET_RTS()
				RESET_RTS()
			if (!(pcs->cblkp->flow_flags & FLOW_OUT_OFF))
				// Enable TX Empty interrupt if outward flow is not turned off
			    OrB(pcs->prescale_reg, UIR_TX);
		}
		OrB(PMXC_IMR_addr,IMR_MIRQ1_EN);
#endif
		Enb();
   	}
#ifdef SNAP
    else {
		Dis();
		if (GetSmallOutbufDataLength(pcs)) {
			if ((pcs->cblkp->flow_flags & FLOW_MODE) == FLOW_ASYMM)
				//Activate RTS if asymmetric RTS/CTS used
//				WrPort(pcs->control_reg + UART_MCR, RdPort(pcs->control_reg + UART_MCR) | MCR_RTS);
				WrPort(pcs->control_reg + UART_MCR, RdPort(pcs->control_reg + UART_MCR) & ~MCR_RTS);
			if (!(pcs->cblkp->flow_flags & FLOW_OUT_OFF))
	   			// Enable Transmitter Buffer Empty Interrupt
				WrPort(pcs->control_reg + UART_IER, RdPort(pcs->control_reg + UART_IER) | IER_TBE);
	   	}
		Enb();
   	}
#endif    

	return length;
}

/*******************************************************************************
* @func		comWaitData
*
* @shortdesc Wait for data.
* @desc		Waits until data available.
*			Will not return until at least on byte is available for reading on
*			the specified port.
*
* @param	int handle Port handle returned by <var>comOpen</var>.
*
* @return	int E_COM_SUCCESS if successful, otherwise negative error code. 
*	
* @see		comDataAvailable
*	
*******************************************************************************/
int comWaitData(int handle)
{
 	COMPORT_SETTINGS	*pcs;
    OS_ERR err;
    //struct timespec rqtp;
	//struct timespec rmtp;


	// Check the handle
    pcs = ComPortList[(handle-1) & HANDLEMASK];
    if (pcs->handle != handle)
    	return E_COM_INVALID_PARAM;

	//Wait for data to become available
	while((GetBigInbufDataLength(pcs) + GetSmallInbufDataLength(pcs)) == 0) {
    	//rqtp.tv_sec = 0;
    	//rqtp.tv_nsec = DELAY_RECV_DATA_MS*1000000;
		//nanosleep (&rqtp, &rmtp);
	    OSTimeDlyHMSM(0u, 0u, 0u, DELAY_RECV_DATA_MS,
                      OS_OPT_TIME_HMSM_STRICT,
                      &err);

	}

	return E_COM_SUCCESS;
}

/*******************************************************************************
* @func		comDataAvailable
*
* @shortdesc Test if data available.
* @desc		Tests if there is any data available to read on specified port.
*
* @param	int handle Port handle returned by <var>comOpen</var>.
*
* @return	int Number of bytes available if successful, otherwise negative error
*			code. 
*	
* @see		comRead
*	
* @see		comWaitData
*	
*******************************************************************************/
int comDataAvailable(int handle)
{
	COMPORT_SETTINGS *pcs;
    int	length,bufsz;
    	
	// Check the handle
    pcs = ComPortList[(handle-1) & HANDLEMASK];
    if (pcs->handle != handle)
    	return E_COM_INVALID_PARAM;

	Dis();
	length = GetBigInbufDataLength(pcs) + GetSmallInbufDataLength(pcs);
	Enb();

	if (length < (bufsz = pcs->inbuf_endptr - pcs->inbuf_bufptr))
		return length;
    else
		return bufsz-1;
}

/*******************************************************************************
* @func		comFlush
*
* @shortdesc Empty the port.
* @desc		Waits for all written data to be sent.
*			Will not return until the output buffer is empty and all data in the
*			UART transmit buffer and shift register is sent.
*
* @param	int handle Port handle returned by <var>comOpen</var>.
*
* @return	int E_COM_SUCCESS if successful, otherwise negative error code. 
*	
* @see		comWrite
*	
*******************************************************************************/
int comFlush(int handle)
{
	COMPORT_SETTINGS	*pcs;		
	
	// Check the handle
    pcs = ComPortList[(handle-1) & HANDLEMASK];
    if (pcs->handle != handle)
    	return E_COM_INVALID_PARAM;
 
	while (1) {
		Dis();
		if (!GetSmallOutbufDataLength(pcs)) break;				// Wait for emtpy buffer
		Enb();
    }
	Enb();

	if (pcs->porttype == INTERNAL_PORT) {
	    while(InB(pcs->status_reg) & USR_TX_NEF);				// Wait for empty FIFO & shift reg
    }
#ifdef SNAP
	else {
		while (!(RdPort(pcs->control_reg + UART_LSR) & LSR_TEMT));	// Wait for empty FIFO & shift reg
    }
#endif
	return E_COM_SUCCESS; 
}

/*******************************************************************************
* @func		comConfig
*	
* @shortdesc Change port settings.
* @desc		Changes the settings of an open port.
*			The port is flushed before settings are changed.
*
* @param	int handle Port handle returned by <var>comOpen</var>.
* @param	int baudrate New baud rate setting for the port.
* @param	int databits New number of data bits setting for the port.
*			Use one of the constants DATABITS_5 .. DATABITS_8.
* @param	int stopbits New number of stop bits setting for the port.
*			Use one of the constants STOPBITS_1 or STOPBITS_2.
* @param	int parity New parity setting for the port.
*			Use one of the constants PARITY_NONE, PARITY_ODD, or PARITY_EVEN.
*
* @return	int E_COM_SUCCESS if successful, otherwise negative error code. 
*	
* @see		comOpen
* @see		comSetBaudrate
* @see		comSetInputBufferSize
* @see		comSetOutputBufferSize
* @see		comSetFlowControlMode
*	
*******************************************************************************/
int comConfig(int handle, int baudrate, int databits, int stopbits, int parity)
{
 	COMPORT_SETTINGS	*pcs;

    // Check the handle
    pcs = ComPortList[(handle-1) & HANDLEMASK];
    if (pcs->handle != handle)
    	return E_COM_INVALID_PARAM;

	// Check parameters
   	if (validateBaudrate(pcs, baudrate) < 0)
		return E_COM_INVALID_PARAM;

	if (validateDatabits(pcs, databits) < 0)
		return E_COM_INVALID_PARAM;

	if (parity!=PARITY_NONE && parity!=PARITY_ODD && parity!=PARITY_EVEN)
		return E_COM_INVALID_PARAM;

	if (stopbits!=STOPBITS_1 && stopbits!=STOPBITS_2)
		return E_COM_INVALID_PARAM;

TRACE0("[comConfig]\n");

	// Parameters was ok
	pcs->baudrate = baudrate;	
	pcs->databits = databits;
	pcs->parity = parity;
	pcs->stopbits = stopbits;

	comFlush(handle);								// Flush port before changing its configuration
	setBaudrate(pcs, pcs->baudrate);				// Set baudrate	
	setCtrlReg(pcs);								// Set port control		
	
	return E_COM_SUCCESS;
}

/*******************************************************************************
* @func		comSetBaudrate
*
* @shortdesc Set baudrate.
* @desc		Changes the baudrate of an open port.
*			The port is flushed before baudrate is changed.
*
* @param	int handle Port handle returned by <var>comOpen</var>.
* @param	int baudrate New baud rate setting for the port.
*
* @return	int E_COM_SUCCESS if successful, otherwise negative error code. 
*	
* @see		comOpen
* @see		comConfig
* @see		comGetBaudrate
*	
*******************************************************************************/
int comSetBaudrate(int handle, int baudrate)
{
 	COMPORT_SETTINGS	*pcs;

TRACE0("[comSetBaudrate]\n");

	// Check the handle
    pcs = ComPortList[(handle-1) & HANDLEMASK];
    if (pcs->handle != handle)
    	return E_COM_INVALID_PARAM;

    // Flush port before changing baudrate
	comFlush(handle);		

	// Set baudrate
	if (setBaudrate(pcs, baudrate) != E_COM_SUCCESS)
    	return E_COM_INVALID_PARAM;
   	pcs->baudrate = baudrate;

	return E_COM_SUCCESS;	
}

/*******************************************************************************
* @func		comGetBaudrate
*
* @shortdesc Get current baudrate.
* @desc		Returns current baudrate setting.
*
* @param	int handle Port handle returned by <var>comOpen</var>.
*
* @return	Current baudrate if successful, otherwise negative error code. 
*	
* @see		comSetBaudrate
*	
*******************************************************************************/
int comGetBaudrate(int handle)
{
 	COMPORT_SETTINGS	*pcs;

	// Check the handle
    pcs = ComPortList[(handle-1) & HANDLEMASK];
    if (pcs->handle != handle)
    	return E_COM_INVALID_PARAM;

    return pcs->baudrate;
}

/*******************************************************************************
* @func		comSetInputBufferSize
*
* @shortdesc Set input buffer size.
* @desc		Sets the input buffer size of an open port.
*			Any data currently in the input buffer are lost. The new size is
*			rounded upwards to a multiple of 256.
*
* @param	int handle Port handle returned by <var>comOpen</var>.
* @param	int bufsize New input buffer size for the port.
*
* @return	int E_COM_SUCCESS if successful, otherwise negative error code. 
*	
* @see		comOpen
*	
* @see		comGetInputBufferSize
*	
*******************************************************************************/
int comSetInputBufferSize(int handle, int bufsize)
{
 	COMPORT_SETTINGS	*pcs;
	unsigned char 		*pNewBuf, *pOldBuf;

	// Check the handle
    pcs = ComPortList[(handle-1) & HANDLEMASK];
    if (pcs->handle != handle)
    	return E_COM_INVALID_PARAM;

	if (bufsize < MINBUFSIZE)			// Don't allow smaller buffers than MINBUFSIZE
    	bufsize = MINBUFSIZE;
	bufsize = (bufsize+255)&0xFFFFFF00;	// Make bufsize a multiple of 256

	//Don't do anything if the new size is the same as the old
	if (bufsize != (pcs->inbuf_endptr - pcs->inbuf_bufptr)) {
		// Allocate a new buffer
    	if (!(pNewBuf = (unsigned char*)malloc(bufsize)))
    		return E_COM_OUT_OF_MEMORY;
		
		// Disable RX interrupts
		if (pcs->porttype == INTERNAL_PORT)		// Internal port
		    AndB(pcs->prescale_reg, ~UIR_RX);
#ifdef SNAP
	    else									// External serial port
			WrPort(pcs->control_reg + UART_IER, RdPort(pcs->control_reg + UART_IER) & ~IER_RBF);		
#endif

		// Switch buffer
	    pOldBuf = pcs->inbuf_bufptr;			// Save old buffer pointer for deallocation
	    pcs->inbuf_bufptr = pNewBuf;
	    pcs->inbuf_inptr = pNewBuf;
    	pcs->inbuf_outptr = pNewBuf;
    	pcs->inbuf_endptr = pNewBuf + bufsize;
		pcs->cblkp->inbuf_inptr = 0;			// Have to reset these to keep the wrap alignment
		pcs->cblkp->inbuf_outptr = 0;

		// Enable RX interrupts again, but only if read access
		if (pcs->porttype == INTERNAL_PORT) {	// Internal port
			if (pcs->access & COM_READ) 
			    OrB(pcs->prescale_reg, UIR_RX);
	    }
#ifdef SNAP
    	else {									// External serial port
			if (pcs->access & COM_READ) 
				WrPort(pcs->control_reg + UART_IER, RdPort(pcs->control_reg + UART_IER) | IER_RBF);		
	    }
#endif

		// Free old buffer
	    if (pOldBuf)
	    	free(pOldBuf);
    }

	return E_COM_SUCCESS;
}

/*******************************************************************************
* @func		comSetOutputBufferSize
*
* @shortdesc Set output buffer size.
* @desc		Sets the output buffer size of an open port.
*			Any data currently in the output buffer are lost. The new size is
*			rounded upwards to a multiple of 256.
*
* @param	int handle Port handle returned by <var>comOpen</var>.
* @param	int bufsize New output buffer size for the port.
*
* @return	int E_COM_SUCCESS if successful, otherwise negative error code. 
*	
* @see		comOpen
*	
* @see		comGetOutputBufferSize
*	
*******************************************************************************/
int comSetOutputBufferSize(int handle, int bufsize)
{
 	COMPORT_SETTINGS	*pcs;
	unsigned char 		*pNewBuf, *pOldBuf;

	// Check the handle
    pcs = ComPortList[(handle-1) & HANDLEMASK];
    if (pcs->handle != handle)
    	return E_COM_INVALID_PARAM;

	if (bufsize < MINBUFSIZE)			// Don't allow smaller buffers than MINBUFSIZE
    	bufsize = MINBUFSIZE;
	bufsize = (bufsize+255)&0xFFFFFF00;	// Make bufsize a multiple of 256

	//Don't do anything if the new size is the same as the old
	if (bufsize != (pcs->outbuf_endptr - pcs->outbuf_bufptr)) {
		// Allocate a new buffer
    	if (!(pNewBuf = (unsigned char*)malloc(bufsize)))
    		return E_COM_OUT_OF_MEMORY;
		
		// Disable TX interrupts
		if (pcs->porttype == INTERNAL_PORT)		// Internal port
		    AndB(pcs->prescale_reg, ~UIR_TX);
#ifdef SNAP
	    else									// External serial port
			WrPort(pcs->control_reg + UART_IER, RdPort(pcs->control_reg + UART_IER) & ~IER_TBE);		
#endif

		// Switch buffer
	    pOldBuf = pcs->outbuf_bufptr;			// Save old buffer pointer for deallocation
	    pcs->outbuf_bufptr = pNewBuf;
	    pcs->outbuf_inptr = pNewBuf;
    	pcs->outbuf_outptr = pNewBuf;
    	pcs->outbuf_endptr = pNewBuf + bufsize;
		pcs->cblkp->outbuf_inptr = 0;			// Have to reset these to keep the wrap alignment
		pcs->cblkp->outbuf_outptr = 0;
		
		// Don't enable TX interrupts again, there's nothing to send

		// Free old buffer
	    if (pOldBuf)
	    	free(pOldBuf);
    }

	return E_COM_SUCCESS;
}

/*******************************************************************************
* @func		comGetInputBufferSize
*
* @shortdesc Get input buffer size.
* @desc		Returns a ports current input buffer size.
*
* @param	int handle Port handle returned by <var>comOpen</var>.
* @return	int Input buffer size if successful, otherwise negative error code. 
*	
* @see		comSetInputBufferSize
*	
*******************************************************************************/
int comGetInputBufferSize(int handle)
{
 	COMPORT_SETTINGS	*pcs;

	// Check the handle
    pcs = ComPortList[(handle-1) & HANDLEMASK];
    if (pcs->handle != handle)
    	return E_COM_INVALID_PARAM;

	return pcs->inbuf_endptr - pcs->inbuf_bufptr;
}

/*******************************************************************************
* @func		comGetOutputBufferSize
*
* @shortdesc Get current output buffer size.
* @desc		Returns a ports the current output buffer size.
*
* @param	int handle Port handle returned by <var>comOpen</var>.
*
* @return	int Output buffer size if successful, otherwise negative error code. 
*	
* @see		comSetOutputBufferSize
*	
*******************************************************************************/
int comGetOutputBufferSize(int handle)
{
 	COMPORT_SETTINGS	*pcs;

	// Check the handle
    pcs = ComPortList[(handle-1) & HANDLEMASK];
    if (pcs->handle != handle)
    	return E_COM_INVALID_PARAM;

	return pcs->outbuf_endptr - pcs->outbuf_bufptr;
}

/*******************************************************************************
* @func		comSetFlowControlMode
*
* @shortdesc Set flow control mode.
* @desc		Sets the flow control mode of an open port.
*			The port is flushed before flow control mode is changed.
*
* @param	int handle Port handle returned by <var>comOpen</var>.
* @param	int flowcontrol New flow control mode for the port.
*			Use one of the constants FLOWCONTROL_NONE, FLOWCONTROL_RTSCTS,
*			FLOWCONTROL_XONXOFF or FLOWCONTROL_ASYMM.
*
* @return	int E_COM_SUCCESS if successful, otherwise negative error code. 
*	
* @see		comOpen
*	
* @see		comGetFlowControlMode
*	
*******************************************************************************/
int comSetFlowControlMode(int handle, int flowcontrol)
{
    COMPORT_SETTINGS* pcs;

	// Check the handle
    pcs = ComPortList[(handle-1) & HANDLEMASK];
    if (pcs->handle != handle)
    	return E_COM_INVALID_PARAM;

	// Check the parameter
	if ((flowcontrol != FLOWCONTROL_NONE) && (flowcontrol != FLOWCONTROL_RTSCTS) &&
		(flowcontrol != FLOWCONTROL_XONXOFF) && (flowcontrol != FLOWCONTROL_ASYMM))
		return E_COM_INVALID_PARAM;

#ifdef SNAP
	// On SNAP, the hardware handshake signals are shared by the internal ports.
	// Only one of them can be the owner of the signals which are claimed by 
	// the comSetRTSCTSFlowControlEnable() function.
    if (((flowcontrol == FLOWCONTROL_RTSCTS) || (flowcontrol == FLOWCONTROL_ASYMM)) &&
		(pcs->porttype == INTERNAL_PORT))
	{
    	if (hwhandshakeOwner != pcs->port)
			return E_COM_INVALID_PARAM;
    }
#endif
	
    // Flush port before changing mode
	comFlush(handle);

	//Change mode
	setFlowFlagsMode(pcs,flowcontrol);

	if (pcs->porttype == INTERNAL_PORT) {
    	AndB(pcs->prescale_reg, ~UIR_STATUS);
		if ((pcs->cblkp->flow_flags & FLOW_MODE) == FLOW_RTSCTS) {
			if (pcs->access & COM_WRITE) 				//If write access and RTS/CTS enable status interrupt
		    	OrB(pcs->prescale_reg, UIR_STATUS);
   			SET_RTS()									//Activate RTS (flow enabled) if RTS/CTS used
	    }
	}
#ifdef SNAP
    else {
		WrPort(pcs->control_reg + UART_IER, RdPort(pcs->control_reg + UART_IER) & ~IER_MS);		
		if ((pcs->cblkp->flow_flags & FLOW_MODE) == FLOW_RTSCTS) {
			if (pcs->access & COM_WRITE) 				//If write access and RTS/CTS enable MS interrupt
				WrPort(pcs->control_reg + UART_IER, RdPort(pcs->control_reg + UART_IER) | IER_MS);		
			WrPort(pcs->control_reg + UART_MCR, RdPort(pcs->control_reg + UART_MCR) | MCR_RTS);	//Activate RTS (flow enabled) if RTS/CTS used
		}
    }
#endif

	return E_COM_SUCCESS;								
}

/*******************************************************************************
* @func		comGetFlowControlMode
*
* @shortdesc Get flow control mode.
* @desc		Gets a ports current flow control mode.
*
* @param	int handle Port handle returned by <var>comOpen</var>.
*
* @return	int Flow control mode if successful, otherwise negative error code. 
*	
* @see		comSetFlowControlMode
*	
*******************************************************************************/
int comGetFlowControlMode(int handle)
{
    COMPORT_SETTINGS* pcs;

	// Check the handle
    pcs = ComPortList[(handle-1) & HANDLEMASK];
    if (pcs->handle != handle)
    	return E_COM_INVALID_PARAM;

    return comGetFlowFlagsMode(pcs);
}

/*******************************************************************************
* @func		comSetIrDAMode
*
* @shortdesc Enable or disable IrDA mode.
* @desc		Enables or disable IrDA mode.
*
* @param	int port Port number of port to enable or disable IrDA mode for.
*			Must be between 1 and the number of available ports, inclusive. The
*			port may be open or closed.
*
* @param	int enable Enable IrDA mode if true, otherwise disable.
*
* @return	int E_COM_SUCCESS if successful, otherwise negative error code. 
*	
*******************************************************************************/
int comSetIrDAMode(int port, int enable)
{
	COMPORT_SETTINGS	*pcs;
	unsigned char modebits;
										   
	// Check port
    if ((port < FIRST_PORT) || (port > LAST_PORT)) 
    	return E_COM_INVALID_PARAM;

   	pcs = ComPortList[port-1];
	if (pcs->porttype != INTERNAL_PORT)
    	return E_COM_UNSUPPORTED_OPERATION;

	modebits = enable ? UCR_IRDA : UCR_NORMAL;
    pcs->mode = enable ? MODE_IRDA : MODE_NORMAL;

	// Set the port mode if the port is already open
	if (pcs->handle != 0) {
	    EnablePort(pcs, modebits);
    }

	return E_COM_SUCCESS;
}

/*******************************************************************************
* @func		comSendBreak
*
* @shortdesc Sends break.
* @desc		Sends break.
*
* @param	int handle Port handle returned by <var>comOpen</var>.
*
* @param	int millis Duration of break in milliseconds.
*
* @return	int E_COM_SUCCESS if successful, otherwise negative error code. 
*	
*******************************************************************************/
int comSendBreak(int handle, int millis)
{
	COMPORT_SETTINGS	*pcs;
    OS_ERR err;
    //struct timespec rqtp;
	//struct timespec rmtp;


	// Check the handle
    pcs = ComPortList[(handle-1) & HANDLEMASK];
    if (pcs->handle != handle)
    	return E_COM_INVALID_PARAM;

	if (pcs->mode != MODE_NORMAL)
    	return E_COM_UNSUPPORTED_OPERATION;

	if (pcs->porttype == INTERNAL_PORT) {
    	EnablePort(pcs, UCR_BREAK);		// Set break mode
    	//rqtp.tv_sec = 0;
    	//rqtp.tv_nsec = millis*1000000;
	    OSTimeDlyHMSM(0u, 0u, 0u, millis,
                      OS_OPT_TIME_HMSM_STRICT,
                      &err);
		//nanosleep (&rqtp, &rmtp);	    // Wait millis

    	EnablePort(pcs, UCR_NORMAL);	// Set normal mode
    }
#ifdef SNAP
	else {
		WrPort(pcs->control_reg + UART_LCR, RdPort(pcs->control_reg + UART_LCR) | LCR_BREAK);	// Set break mode
		osthread_sleep_ms(millis);	// Wait millis
		WrPort(pcs->control_reg + UART_LCR, RdPort(pcs->control_reg + UART_LCR) & ~LCR_BREAK);	// Set normal mode
    }
#endif

	return E_COM_SUCCESS;
}

/*******************************************************************************
* @func		comSetBit
*
* @shortdesc Set port signal.
* @desc		Sets the state of a port signal.
*
* @param	int handle Port handle returned by <var>comOpen</var>.
*
* @param	int bit Bit to affect.
*			Use one of the constants BIT_RTS or BIT_DTR.
*
* @param	int value State to set the bit to.
*			If true, the bit is set to active (low TTL level, positive RS232
*			level). If false, the bit is set to inactive (high TTL level,
*			negative RS232 level). 
*
* @see		comGetBit
*	
*******************************************************************************/
void comSetBit(int handle, int bit, int value)
{
 	COMPORT_SETTINGS	*pcs;
	unsigned char mask;

	// Check the handle
    pcs = ComPortList[(handle-1) & HANDLEMASK];
    if (pcs->handle != handle)
    	return;

#ifdef SNAP
    if (pcs->porttype == INTERNAL_PORT) {
		// On SNAP, the hardware handshake signals are shared by the internal ports.
		// Only one of them can be the owner of the signals which are claimed by 
		// the comSetRTSCTSFlowControlEnable() function.
    	if (hwhandshakeOwner != pcs->port)
			return;

		if (bit == BIT_RTS) {
			if (value)
    			SET_RTS()
	    	else
		    	RESET_RTS()
	    }
	    else if (bit == BIT_DTR) {
		    if (value)
				SET_DTR()
		    else
		    	RESET_DTR()
	    }
    }
    else {
    	// External port
    	if (bit == BIT_RTS)
			mask = MCR_RTS;		  
    	else if (bit == BIT_DTR)
			mask = MCR_DTR;		  
		else
	    	return;

	    if (value) 	    	
			WrPort(pcs->control_reg + UART_MCR, RdPort(pcs->control_reg + UART_MCR) | mask);	// Set
	    else 	    	
			WrPort(pcs->control_reg + UART_MCR, RdPort(pcs->control_reg + UART_MCR) & ~mask);	// Clear
    }

#else	
	// Not SNAP
	if (bit == BIT_RTS) {
		if (value)
	    	SET_RTS()
	    else
    		RESET_RTS()
	}
#endif
}

/*******************************************************************************
* @func		comGetBit
*
* @shortdesc Get port signal.
* @desc		Gets the state of a port signal.
*
* @param	int handle Port handle returned by <var>comOpen</var>.
*
* @param	int bit Bit to get.
*			Use one of the constants BIT_RTS, BIT_DTR, BIT_CTS, BIT_CD, BIT_RI,
*			or BIT_DSR.
*
* @return	int Current state of the specified bit.
*			Polarity is defined the same way as for comSetBit.
*	
* @see		comSetBit
*	
*******************************************************************************/
int comGetBit(int handle, int bit)
{
 	COMPORT_SETTINGS	*pcs;
    unsigned char status, mask;

	// Check the handle
    pcs = ComPortList[(handle-1) & HANDLEMASK];
    if (pcs->handle != handle)
    	return E_COM_INVALID_PARAM;

#ifdef SNAP
    if (pcs->porttype == INTERNAL_PORT) {
    	// Internal port
		// On SNAP, the hardware handshake signals are shared by the internal ports.
		// Only one of them can be the owner of the signals which are claimed by 
		// the comSetRTSCTSFlowControlEnable() function.
	   	if (hwhandshakeOwner != pcs->port)
			return -1;

    	if (bit == BIT_RTS)
			READ_RTS(status)
	    else if (bit == BIT_DTR)
			READ_DTR(status)
		else if (bit == BIT_CTS)
			READ_CTS(status)
		else if (bit == BIT_CD)
			READ_CD(status)
	    else if (bit == BIT_RI)
	    	status = 0;
	    else if (bit == BIT_DSR)
	    	status = 0;
	    else
    		return E_COM_INVALID_PARAM;
    }
    else {
		// External port
    	if (bit == BIT_RTS) {
	    	return (RdPort(pcs->control_reg + UART_MCR) & MCR_RTS) != 0 ? 1 : 0;
		}
	    else if (bit == BIT_DTR) {
	    	return (RdPort(pcs->control_reg + UART_MCR) & MCR_DTR) != 0 ? 1 : 0;
		}

    	if (bit == BIT_CTS)
			mask = MSR_CTS;		  
    	else if (bit == BIT_CD)
			mask = MSR_CD;		  
    	else if (bit == BIT_DSR)
			mask = MSR_DSR;		  
    	else if (bit == BIT_RI)
			mask = MSR_RI;		  
		else
	    	return -1;
		status = RdPort(pcs->control_reg + UART_MSR) & mask;
    }

#else
	// Not SNAP
	if (bit == BIT_RTS) 
		READ_RTS(status)
	else if (bit == BIT_CTS) 
		READ_CTS(status)
    else
   		return E_COM_INVALID_PARAM;
#endif	

	return status != 0 ? 1 : 0;
}

/*******************************************************************************
* @func		comSetReceiveFraming
*
* @shortdesc Enable or disable receive framing.
* @desc		Enables or disables receive framing.
*			When receive framing is enabled, a call to comRead will only return
*			the characters up to and including the next framing character, even
*			if there are more data available and the length parameter to comRead
*			allows more data to be read.
*
* @param	int handle Port handle returned by <var>comOpen</var>.
*
* @param	int framingByte Framing character.
*			-1 disables receive framing, any other value enables it with the
*			specified value as framing character.
*
* @return	int E_COM_SUCCESS if successful, otherwise negative error code. 
*	
* @see		comRead
*	
* @see		comGetReceiveFraming
*	
*******************************************************************************/
int	comSetReceiveFraming(int handle, int framingByte)	
{
	COMPORT_SETTINGS	*pcs;

	// Check the handle and millis
    pcs = ComPortList[(handle-1) & HANDLEMASK];
    if (pcs->handle != handle)
    	return E_COM_INVALID_PARAM;

	if (framingByte >= 0) 
    	pcs->rcvFramingByte = framingByte & 0xff;
    else if (framingByte == -1)
    	pcs->rcvFramingByte = -1;
    else
    	return E_COM_INVALID_PARAM;

	return E_COM_SUCCESS;
}

/*******************************************************************************
* @func		comGetReceiveFraming
*
* @shortdesc Get framing character.
* @desc		Gets a ports current framing character.
*
* @param	int handle Port handle returned by <var>comOpen</var>.
*
* @return	int Current framing character if successful and framing is enabled
*			for the port, otherwise negative error code. 
*	
* @see		comSetReceiveFraming
*	
*******************************************************************************/
int	comGetReceiveFraming(int handle)
{
	COMPORT_SETTINGS	*pcs;

	// Check the handle
    pcs = ComPortList[(handle-1) & HANDLEMASK];
    if (pcs->handle != handle)
    	return E_COM_INVALID_PARAM;

    return pcs->rcvFramingByte;
}

/*******************************************************************************
* @func		comSetRTSCTSFlowControlEnable
*
* @mod		COM 
*	
* @desc		Sets ownership of the hardware handshake signals for a port.
*			Only necessary for internal ports, external ports all have separate
*			sets of hardware handshake signals.
*
* @param	int portNum Port number of port to set ownership for.
*			Note that this port number is zero-based, in contrast to the port
*			number supplied to <var>comOpen</var> which is one-based. To set ownership for
*			serial0, use port number 0.
*
* @param	int enable Take ownership if true, otherwise release ownership.
*
* @return	int A negative error code on error, else 1 if successful or zero if
*			another internal port already owns the hardware handshaking lines. 
*	
* @see		comOpen
* @see		comGetRTSCTSFlowControlEnable
*	
*******************************************************************************/
int comSetRTSCTSFlowControlEnable(int portNum, int enable)
{
	COMPORT_SETTINGS *pcs;

	// Check port number.
	if ((portNum < 0) || (portNum >= NUMCOMPORTS))
    	return E_COM_INVALID_PARAM;

	pcs = ComPortList[portNum];

	// External always have access to hardware handshake signals.
    if (pcs->porttype == EXTERNAL_PORT)
    	return 1;

	if (hwhandshakeOwner==pcs->port) {
		if (!enable) {
	    	// Disable hardware handshake on the port
	    	if ((pcs->cblkp->flow_flags & FLOW_MODE) == FLOWCONTROL_RTSCTS)
		    	comSetFlowControlMode(pcs->handle, FLOWCONTROL_NONE);
	    	hwhandshakeOwner = -1;
		}	    	
	}	
    else if (hwhandshakeOwner != -1) {
    	// The flow control is owned by another port
    	return 0;
    }
    else if (!enable) {
    	// Release wanted but the control was not owned by any.
	    return 1;
    }
    else {
    	// Make portNum the owner of the hardware handshake signals
	    hwhandshakeOwner = pcs->port;

//    	// Set up the directions of the hardware handshake signals
//	 	AndDJ(~(EXP_OUT1 | EXP_IN1));	// 
//		OrCJ(EXP_OUT1);   				// Configure OUT1 (RTS) line as output
//		AndCJ(~EXP_IN1);   				// Configure IN1 (CTS) line as input
    }

	return 1;
}

/*******************************************************************************
* @func		comGetRTSCTSFlowControlEnable
*
* @mod		COM 
*	
* @desc		Test if port owns the hardware handskake lines.
*
* @param	int portNum Port number of port to test.
*			Note that this port number is zero-based, in contrast to the port
*			number supplied to <var>comOpen</var> which is one-based. To test serial0, use
*			port number 0.
*
* @return	int true if the specified port is the owner of the hardware handshake
*			signals, otherwise false. 
*	
* @see		comSetRTSCTSFlowControlEnable
*******************************************************************************/
int comGetRTSCTSFlowControlEnable(int portNum)
{
	COMPORT_SETTINGS *pcs;

	// Check port number.
	if ((portNum < 0) || (portNum >= NUMCOMPORTS))
    	return 0;

	pcs = ComPortList[portNum];

	// serial2 and serial3 always have access to hardware handshake signals.
    if (pcs->porttype == EXTERNAL_PORT)
    	return 1;

	return hwhandshakeOwner==pcs->port;
}




