;
; comhandler.asm - Interrupt handler for serial ports.
;
; Copyright (C) 2001 Imsys AB. Allrights reserved.
;
;===============================================================================

	SEGMENT COMHANDLER, CODE

;===============================================================================

	INCL	"io_macro.inc"
	INCL	"uart_defs.inc"
	INCL	"comdef.inc"

;===============================================================================
; Macros
SET_EVENT:	MACRO	%1
        ld	EVENT_FLAGS	        
        dup
        ld.b	[v0+es]
        ori	%1
        st.b	[v0+es]
ENDM

SET_ERROR:	MACRO	%1
        ld	ERROR_CODE
        ld	%1
	st	[v0+es]	
ENDM

;===============================================================================
; Externals
EXTRN _ComPort1

;;===============================================================================
;; _enableComInterrupt
;;
;; Enables com port interrupt and installs handler.
;;
;; C syntax:	void enableComInterrupt(void)
;;
;;===============================================================================
;_enableComInterrupt::
;	;Init interrupt vector
;	ld	COM_ISR_ADR
;	ld.a	comHandler
;	st
;
;	;Set UART mask to level 6
;	ld	COM_ISR
;        ld	COM_IRQ_MASK
;        st.b
;
;	;Set MIRQ1_EN in PMXC_IMR
;        ld      PMXC_IMR_addr
;        ld      IMR_MIRQ1_EN
;	or.bus.b
;
;        ret

;===============================================================================
; _emptyRX
;
; Empties small input buffer into big input buffer.
;
; C syntax:	void emptyRX(COMPORT_SETTINGS *pcs)
;
;===============================================================================
_emptyRX::
        push    rar
	als	3

	call	com_saveptrs	;Save pcs in v0 and get cblk ptr to v1	

	dis			;Disable interrupts while emptying RX buffer
	call	com_rx_empty	;Empty RX buffer
	enb			;Enable interrupts again

	dls	3
        pop    rar
        ret

;===============================================================================
; _fillTX
;
; Fills small output buffer from big output buffer.
;
; C syntax:	void fillTX(COMPORT_SETTINGS *pcs)
;
;===============================================================================
_fillTX::
        push    rar
	als	3

	call	com_saveptrs	;Save pcs in v0 and get cblk ptr to v1	

	dis			;Disable interrupts while filling TX buffer
	call	com_tx_fill     ;Fill TX buffer
	enb			;Enable interrupts again

	dls	3
        pop    rar
        ret

;===============================================================================
; _getEvent
;
; Returns the event flags, both microprogram generated and assembler generated.
; Also clears all event flags before returning.
;
; C syntax:	int getEvent(COMPORT_SETTINGS *pcs)
;
;===============================================================================
_getEvent::
	dup			;stack: ..., ^pcs, ^pcs
	addi	EVENT_FLAGS     ;stack: ..., ^pcs, ^event_flags
	dup		        ;stack: ..., ^pcs, ^event_flags, ^event_flags
	dis
	ld.b		        ;stack: ..., ^pcs, ^event_flags, event_flags
	swap		        ;stack: ..., ^pcs, event_flags, ^event_flags
	ld	0	        ;Clear event_flags
	st.b		        ;stack: ..., ^pcs, event_flags
	enb
	swap		        ;stack: ..., event_flags, ^pcs
	addi	CBLKP	        ;stack: ..., event_flags, ^cblkp
	ld		        ;stack: ..., event_flags, ^cblk
	addi	FLOW_FLAGS      ;stack: ..., event_flags, ^flow_flags
	dup		        ;stack: ..., event_flags, ^flow_flags, ^flow_flags
	ld.b		        ;stack: ..., event_flags, ^flow_flags, flow_flags
	andi	FLOW_EVENT_TX | FLOW_EVENT_RX	;Check flags
	br.nz	com_get_any	;Jump if any flags set
	
	drop2		        ;stack: ..., event_flags
	ret

com_get_any:
	xori	FLOW_EVENT_TX	;Check flags
	br.nz	com_get_rx	;Jump if RX flag set or TX flag not set

	;Here when TX flag was set, but not RX flag
	drop		        ;stack: ..., event_flags, ^flow_flags
	andm	0xFF & ~FLOW_EVENT_TX  ;Clear FLOW_EVENT_TX flag
	ori	EVENT_TX        ;stack: ..., event_flags | EVENT_TX
	ret

com_get_rx:
	xori	FLOW_EVENT_TX | FLOW_EVENT_RX	;Check flags
	br.nz	com_get_both	;Jump if TX flag set

	;Here when RX flag was set, but not TX flag
	drop		        ;stack: ..., event_flags, ^flow_flags
	andm	0xFF & ~FLOW_EVENT_RX  ;Clear FLOW_EVENT_RX flag
	ori	EVENT_RX        ;stack: ..., event_flags | EVENT_RX
	ret

com_get_both:
	;Here when both RX and TX flags were set
	drop		        ;stack: ..., event_flags, ^flow_flags
	andm	0xFF & ~FLOW_EVENT_TX & ~FLOW_EVENT_RX	;Clear FLOW_EVENT_TX and FLOW_EVENT_RX flag
	ori	EVENT_TX | EVENT_RX	;stack: ..., event_flags | EVENT_TX | EVENT_RX
	ret

;===============================================================================
; _setEvent
;
; Sets the event flags, both microprogram generated and assembler generated.
;
; C syntax:	void setEvent(COMPORT_SETTINGS *pcs, int flags)
;
;===============================================================================
_setEvent::
	dup2			;stack: ..., flags, ^pcs, flags, ^pcs
	addi	EVENT_FLAGS     ;stack: ..., flags, ^pcs, flags, ^event_flags
	swap			;stack: ..., flags, ^pcs, ^event_flags, flags
	andi	~EVENT_TX & ~EVENT_RX	;stack: ..., flags, ^pcs, ^event_flags, masked flags
	st.b		        ;stack: ..., flags, ^pcs
	addi	CBLKP	        ;stack: ..., flags, ^cblkp
	ld		        ;stack: ..., flags, ^cblk
	addi	FLOW_FLAGS      ;stack: ..., flags, ^flow_flags
	swap			;stack: ..., ^flow_flags, flags
	andi	EVENT_TX | EVENT_RX	;Check flags
	br.nz	com_set_any	;Jump if any flags set

	;Here none of TX or RX flags were set
	drop		        ;stack: ..., ^flow_flags
	andm	0xFF & ~FLOW_EVENT_TX & ~FLOW_EVENT_RX	;Clear FLOW_EVENT_TX and FLOW_EVENT_RX flag
	ret

com_set_any:
	xori	EVENT_TX	;Check flags
	br.nz	com_set_rx	;Jump if RX flag set or TX flag not set

	;Here when TX flag was set, but not RX flag
	drop		        ;stack: ..., ^flow_flags
	dup		        ;stack: ..., ^flow_flags, ^flow_flags
	orm	FLOW_EVENT_TX	;Set FLOW_EVENT_TX flag
	andm	~FLOW_EVENT_RX	;Clear FLOW_EVENT_RX flag
	ret

com_set_rx:
	xori	EVENT_TX | EVENT_RX	;Check flags
	br.nz	com_set_both	;Jump if TX flag set

	;Here when RX flag was set, but not TX flag
	drop		        ;stack: ..., ^flow_flags
	dup		        ;stack: ..., ^flow_flags, ^flow_flags
	andm	0xFF & ~FLOW_EVENT_TX	;Clear FLOW_EVENT_TX flag
	orm	FLOW_EVENT_RX	;Set FLOW_EVENT_RX flag
	ret

com_set_both:
	;Here when both RX and TX flags were set
	drop		        ;stack: ..., ^flow_flags
	orm	FLOW_EVENT_TX | FLOW_EVENT_RX	;Set FLOW_EVENT_TX and FLOW_EVENT_RX flag
	ret

;===============================================================================
; comhandler
;
; Interrupt routine for serial port communication.
;
;===============================================================================
PUBLIC _comHandler
_comHandler:
;	;>>>> [TEST >>>>
;	ld	DB_index
;        ld	0x10
;	or.port
;	;<<<< [TEST <<<<
	als	3
        push    rar

	;Get the interrupt buffer input and output pointers
        ;and compare them to determine if there are any
        ;interrupts left to process.
com_checkirq:
	ld	IRQ_INPTR
        ld.s			;Read both pointer at once
        dup
        dup
        swab.s
        xor			;Compare pointers
        drop
        br.nz	com_moreirq     ;Jump if not equal

;-----------------------------------------------------------
;	Here when no more interrupts to process.
;-----------------------------------------------------------
	drop
	pop	rar
	dls	3
;	;>>>> [TEST >>>>
;	ld	DB_index
;        ld	0xef
;	and.port
;	;<<<< [TEST <<<<
        ret

;-----------------------------------------------------------
;	Here when there are interrupts in the buffer
;-----------------------------------------------------------
com_moreirq:
	;Add two to interrupt buffer output pointer and write it back
	dup
        addi	2
	ld	IRQ_OUTPTR
        swap
        st.b

	;Read the interrupt buffer entry pointed to by the output pointer
        ;before it was incremented
        ;Entry contains port number followed by interrupt cause
        andi	0xFF
	ld	COM_IRQ_BASE
        add
	ld.s

	;Convert port number to settings ptr in v0 and cblk ptr in v1
	dup
        swab.s
	andi	0xFE		;Bit 0 should always be zero
	dup
	call	com_getsettings
	swap

	;Check interrupt cause
        andi	0xFF
        addi	-3
        br.z	com_rx		;RX interrupt if cause was 3

	addi	1
        br.z	com_tx		;TX interrupt if cause was 2

;-----------------------------------------------------------
; 	Here when error interrupt
;-----------------------------------------------------------
	nip			;Nip away port number
	addi	2		;Restore interrupt cause, equals status register

        ;Check error flags, none set means delta-CTS
        andi	USR_FRAMING_ERR | USR_OVERRUN_ERR | USR_PARITY_ERR
        br.z	com_check_cts	;stack: ..., masked status

	;Test if framing error or break
        dup
        andi	USR_FRAMING_ERR
        drop
        br.z	com_err_10

        ;Framing error or break.
	;Read data. If 0 it is a BREAK and not a framing error.
	ld	DATA_REG
	ld.b	[v0+es]
	in.bus.b		;stack: ..., masked status, data
	ori	0
	drop
	br.nz	com_err_framing	;Jump if framing error	

	;BREAK
        SET_EVENT	EVENT_BRK	
        br	com_check_cts

	;Framing error
com_err_framing:
        SET_ERROR	E_COM_FRAMING_ERROR 	
        SET_EVENT	EVENT_FRM	
        br	com_check_cts

com_err_10:
        dup
        andi	USR_OVERRUN_ERR
        drop
        br.z	com_err_20

        ;Overrun error
        SET_ERROR	E_COM_INPUT_BUFFER_OVERFLOW	
        SET_EVENT	EVENT_OVR	
        br	com_check_cts

com_err_20:
        ;Parity error
        SET_ERROR	E_COM_PARITY_ERROR	
        SET_EVENT	EVENT_PRT	

com_check_cts:
	;If hardware handshake is on, check CTS state
	drop			;drop masked status
	ld	FLOW_FLAGS
        ld.b	[v1+es]
        andi	FLOW_MODE
        xori	FLOW_RTSCTS
        br.z	com_cts		;Jump if RTS/CTS flow control on

        xori	FLOW_RTSCTS | FLOW_ASYMM
        br.z	com_cts		;Jump if asymmetric RTS/CTS flow control on

	drop
        jump   	com_checkirq	;Jump if hardware handshake not on

com_cts:
	drop

	;Set flow_out_off flag
	ld	v1
	addi	FLOW_FLAGS
        orm	FLOW_OUT_OFF

	;Disable TX interrupt
	ld	PRESCALE_REG
        ld.b	[v0+es]
        ld	~UIR_TX
	and.bus.b

	;Check CTS state
	ld	CONTROL_REG
        ld.b	[v0+es]
	in.bus.b
	andi	UCR_CTS
        drop
        br.nz	com_checkirq	;Jump if CTS bit set (flow off), leave TX interrupt disabled

	;CTS bit is not set (flow on), clear flow_out_off flag
	ld	v1
	addi	FLOW_FLAGS
        andm	~FLOW_OUT_OFF

	;Turn on TX interrupt if anything to send
	ld	OUTBUF_OUTPTR
        ld.b	[v1+es]		;stack: ..., outbuf_outptr
	ld	OUTBUF_INPTR
        ld.b	[v1+es]		;stack: ..., outbuf_outptr, outbuf_inptr
	ifc.e	com_checkirq	;Jump if nothing to send, leave TX interrupt disabled

	;Turn on TX interrupt
	ld	PRESCALE_REG
        ld.b	[v0+es]
        ld	UIR_TX
	or.bus.b
        jump	com_checkirq	

;-----------------------------------------------------------
; 	Here when rx interrupt
;-----------------------------------------------------------
com_rx:
	;Calculate pointer to rx buffer
	drop
	addi	COM_BUF_BASE+1

	call	com_rx_empty_1

        jump	com_checkirq	

;-----------------------------------------------------------
; 	Here when tx interrupt
;-----------------------------------------------------------
com_tx:
	;Calculate pointer to tx buffer
	drop
	addi	COM_BUF_BASE

	call	com_tx_fill_1

        jump	com_checkirq	

;===============================================================================
; com_rx_empty
;
; Routine for transferring data from small input buffer to big input buffer.
; Used by _emptyRX function and comhandler.
;
;===============================================================================
com_rx_empty::
	ld	PORT		;Get port number (1..16)
        ld.b	[v0+es]
	shli.b	1	        ;Calculate pointer to rx buffer
	addi	COM_BUF_BASE-1

com_rx_empty_1:
        swab.s
        st	v2		;Save in v2

	;Get destination pointer in big buffer and source pointer
        ;in small buffer to stack
	ld	BIG_INBUF_INPTR
        ld	[v0+es]
        dup
	ld	INBUF_OUTPTR
        ld.b	[v1+es]		
	andi	0xFF
	ld	v2
	add			;stack: ..., dest ptr, dest ptr, src ptr

	;Calculate number of bytes in the small buffer
	ld	INBUF_INPTR
        ld.b	[v1+es]
        over
	sub
        andi	0xFF		;stack: ..., dest ptr, dest ptr, src ptr, bytes

	;Calculate available space in the big buffer
	ld	BIG_INBUF_OUTPTR
        ld	[v0+es]
	addi	-1
	ld	BIG_INBUF_INPTR
        ld	[v0+es]
	sub
        br.c	com_rx_notneg	;stack: ..., dest ptr, dest ptr, src ptr, bytes, space
	ld	BIG_INBUF_ENDPTR	   
        ld	[v0+es]
	add	
	ld	BIG_INBUF_BUFPTR
        ld	[v0+es]
	sub			;stack: ..., dest ptr, dest ptr, src ptr, bytes, space	

	;Check if available bytes fit in big buffer, otherwise change
        ;byte count to the number of bytes that fit
com_rx_notneg:
	over
        sub
        br.c	com_rx_bytesfit
	add
        br	com_rx_bytesok
com_rx_bytesfit:
        drop			;stack: ..., dest ptr, dest ptr, src ptr, bytes

	;Check if the whole data can be transferred without buffer wrap,
        ;by comparing the byte count to the inverted small buffer pointer
com_rx_bytesok:
	dup
	ld	INBUF_OUTPTR
        ld.b	[v1+es]
	andi	0xFF
	xori	0xFF		
	addi	1	        ;This will give the wrap length
        sub
        br.nc	com_rx_nowrap	;stack: ..., dest ptr, dest ptr, src ptr, bytes, bytes - wrap length

	;Here when data wraps in the small buffer, and possibly also in the
	;big one, do the first part of the transfer 
	dup
	st	v2		;Save second bytes in v2
	sub			;stack: ..., dest ptr, dest ptr, src ptr, bytes

	tuck2		        ;stack: ..., dest ptr, src ptr, bytes, dest ptr, src ptr, bytes
        bcopy		        ;stack: ..., dest ptr, src ptr, bytes

	swap		        ;stack: ..., dest ptr, bytes, src ptr
	ori	0xFF	        ;Clear low byte of src ptr
	xori	0xFF
	nrot			;stack: ..., new src ptr, dest ptr, bytes
        add		        ;Add bytes to dst ptr
	dup
	ld	BIG_INBUF_ENDPTR
        ld	[v0+es]
	sub
        drop
        br.nz	com_rx_nobigwrap ;Wrap in big buffer as well?
        drop		   	;Yes, replace new dst ptr by buffer start
	ld	BIG_INBUF_BUFPTR
        ld	[v0+es]
com_rx_nobigwrap:
	tuck			;stack: ..., new dest ptr, new src ptr, new dest ptr
	swap			;stack: ..., dest ptr, dest ptr, src ptr
        ld	v2	        ;stack: ..., dest ptr, dest ptr, src ptr, bytes
	ld	0		;stack: ..., dest ptr, dest ptr, src ptr, bytes, 0

	;Here we have pointers and length on stack, do the transfer, then
        ;update the pointers and write them back
com_rx_nowrap:
	drop			;stack: ..., dest ptr, dest ptr, src ptr, bytes
	tuck2		        ;stack: ..., dest ptr, src ptr, bytes, dest ptr, src ptr, bytes
        bcopy		        ;stack: ..., dest ptr, src ptr, bytes
	tuck		        ;stack: ..., dest ptr, bytes, src ptr, bytes
        add			;Add bytes to src ptr 			
	ld	INBUF_OUTPTR
	swap
        st.b	[v1+es]		;Write back
        add		        ;Add bytes to dst ptr
	ld	BIG_INBUF_INPTR
	swap
        st	[v0+es]	        ;Write back
	ret

;===============================================================================
; com_tx_fill
;
; Routine for transferring data from big output buffer to small output buffer.
; Used by _fillTX function and comhandler.
;
;===============================================================================
com_tx_fill::
	ld	PORT		;Get port number (1..16)
        ld.b	[v0+es]
	shli.b	1	        ;Calculate pointer to tx buffer
	addi	COM_BUF_BASE-2

com_tx_fill_1:
        swab.s
        st	v2		;Save in v2

	;Get source pointer in big buffer and destination pointer
        ;in small buffer to stack
	ld	OUTBUF_INPTR
        ld.b	[v1+es]		
	andi	0xFF
	ld	v2
	add			
        dup
	ld	BIG_OUTBUF_OUTPTR
        ld	[v0+es]	        ;stack: ..., dest ptr, dest ptr, src ptr

	;Calculate available space in the small buffer
        over
	ld	OUTBUF_OUTPTR
        ld.b	[v1+es]
        sub
	xori	0xFF
        andi	0xFF		;stack: ..., dest ptr, dest ptr, src ptr, space

	;Calculate number of bytes in the big buffer
	over
	ld	BIG_OUTBUF_INPTR
        ld	[v0+es]
	swap
	sub
        br.c	com_tx_notneg	;stack: ..., dest ptr, dest ptr, src ptr, space, bytes
	ld	BIG_OUTBUF_ENDPTR	   
        ld	[v0+es]
	add	
	ld	BIG_OUTBUF_BUFPTR
        ld	[v0+es]
	sub			;stack: ..., dest ptr, dest ptr, src ptr, space, bytes	

	;Check if available space can be filled from big buffer, otherwise change
        ;byte count to the number of bytes available
com_tx_notneg:
	over
        sub
        br.c	com_tx_bytesavailable
	add
        br	com_tx_bytesok
com_tx_bytesavailable:
        drop			;stack: ..., dest ptr, dest ptr, src ptr, space

	;Check if the whole data can be transferred without buffer wrap,
        ;by comparing the byte count to the inverted small buffer pointer
com_tx_bytesok:
	dup
	ld	OUTBUF_INPTR
        ld.b	[v1+es]
	andi	0xFF
	xori	0xFF		
	addi	1	        ;This will give the wrap length
        sub
        br.nc	com_tx_nowrap	;stack: ..., dest ptr, dest ptr, src ptr, space, space - wrap length

	;Here when data wraps in the small buffer, and possibly also in the
	;big one, do the first part of the transfer 
	dup
	st	v2		;Save second bytes in v2
	sub			;stack: ..., dest ptr, dest ptr, src ptr, space

	tuck2		        ;stack: ..., dest ptr, src ptr, space, dest ptr, src ptr, space
	bcopy		        ;stack: ..., dest ptr, src ptr, space
;	BCOPY1

        add			;Add space to src ptr 			
	swap			;stack: ..., new src ptr, dest ptr
	ori	0xFF	        ;Clear low byte of dst ptr
	xori	0xFF
	over
	ld	BIG_OUTBUF_ENDPTR
        ld	[v0+es]
	sub
        drop
        br.nz	com_tx_nobigwrap ;Wrap in big buffer as well?
        nip		   	;Yes, replace new src ptr by buffer start
	ld	BIG_OUTBUF_BUFPTR
        ld	[v0+es]
        swap
com_tx_nobigwrap:
	tuck			;stack: ..., new dest ptr, new src ptr, new dest ptr
	swap			;stack: ..., dest ptr, dest ptr, src ptr
        ld	v2	        ;stack: ..., dest ptr, dest ptr, src ptr, space
	ld	0		;stack: ..., dest ptr, dest ptr, src ptr, space, 0

	;Here we have pointers and length on stack, do the transfer, then
        ;update the pointers and write them back
com_tx_nowrap:
	drop			;stack: ..., dest ptr, dest ptr, src ptr, space
	tuck2		        ;stack: ..., dest ptr, src ptr, space, dest ptr, src ptr, space
	bcopy		        ;stack: ..., dest ptr, src ptr, space
;	BCOPY2
	tuck		        ;stack: ..., dest ptr, space, src ptr, space
        add			;Add space to src ptr 			
	ld	BIG_OUTBUF_OUTPTR
	swap
        st	[v0+es]	        ;Write back
        add		        ;Add bytes to dst ptr
	ld	OUTBUF_INPTR
	swap
        st.b	[v1+es]		;Write back
	ret

;===============================================================================
; com_getsettings, com_saveptrs
;
; Routines for converting port number or settings pointer on the stack
; into settings pointer in v0 and control block pointer in v1.
;
;===============================================================================
com_getsettings:		;stack: ..., port number
	;Convert port number in interrupt buffer entry (zero based,
        ;multiplied by two) into a pointer to the port's settings block
        ld	SETTINGS_SZ/2
        mul
        ld.a	_ComPort1
	add			;stack: ..., ^settings	

com_saveptrs:
        ;Save settings pointer in v0. Also get the control block
        ;pointer from the settings block and save it in v1.
        dup
	st	v0
        addi	CBLKP
        ld
	st	v1		;stack: ...

        ret

;===============================================================================
 
