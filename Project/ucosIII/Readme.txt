====================================================================================
FlashIt - Application to setup the boot sector and initialize the flash file system. 
====================================================================================

FlashIt searches in folders with predefined names on the RAM disk for files to install. 

All folders except SNP must contain at most one single file. The names of the files to
install are not significant. To skip installation of a certain file type, remove its
folder or leave the folder empty. 

!!! NOTE !!! This should typically not used by module customers! !!! NOTE !!!
Installation of HIS, NETINFO, RBB_MPGM_BOOTLOADER, RBB_MPGM or RBB_BOOTLOADER
(options -h, -n, -r respectively), or using options -w or -z (), requires that
the write protection of the boot sector is disabled. See the profile help for
more information about flash write protection.


Folder			File
===============================================================================
/HIS			File that contains the HIS (Hardware Identification 
			String) to be stored in the RBB block. The string should
		        contain max 32 characters.
/NETINFO		File that contains the MAC address to be stored in the
			RBB block. For example: fc:af:6a:01:02:03.
/RBB_MPGM_BOOTLOADER    The read-only microprogram boot loader. Two options,
			depending on flash size. Choose the right one!
/RBB_MPGM		The read-only microprogram.
/RBB_BOOTLOADER		The read-only assembly boot loader.
/SNP			snp-files to be installed in the flash file system. This
			folder can contain more than one snp file to install.

The program can be started with a combination of the following options:

Options
===============================================================================
-i	Print current boot information. Nothing will be written to flash.
-w	Write protect the flash. Set for first time initialization only.
-z	Reset boot sector. Should be set for first time initialization.
-r	Write read-only files. Implied with -z. Used to change RBB programs.
-n	Write NETINFO. Implied with -z. Used to change MAC address.
-h	Write HIS. Implied with -z. Used to change Hardware Identification string.
-f	Format flash. Needed for first time initialization of the file system.
-c1	Output messages to COM1.
-c2	Output messages to COM2.
-c3	Output messages to COM3.
-d	Output messages to Developer's Output window.


