;********************************************************************************************************
;                                               uC/OS-III
;                                        The Real-Time Kernel
;
;                            (c) Copyright 1998-2015, Micrium, Inc., Weston, FL
;                    All rights reserved.  Protected by international copyright laws.
;
;                                       ASSEMBLY LANGUAGE PORT
;
;                                      $$$$ Processor Name
;                                      $$$$ Compiler/Assembler Name
;
; Filename     : os_cpu_a.asm
; Version      : $$$$ V3.05.01
; By           : $$$$ JJL
;********************************************************************************************************

                                         ; Include macros from 'os_cpu_a.inc'
;    #INCL  "os_cpu_a.inc"

;********************************************************************************************************
;                                           PUBLIC FUNCTIONS
;********************************************************************************************************

    PUBLIC   _OSStartHighRdy             ; Public functions
    PUBLIC   _OSCtxSw
    PUBLIC   _OSIntCtxSw
    PUBLIC   _OSTickISR
    PUBLIC   _CTXSW_SR

    EXTRN   _OSTCBCurPtr                ; Declared as OS_TCB *               , 32-bit long
    EXTRN   _OSTCBHighRdyPtr            ; Declared as OS_TCB *               , 32-bit long
    EXTRN   _OSPrioCur                  ; Declared as INT8U                  ,  8-bit long
    EXTRN   _OSPrioHighRdy              ; Declared as INT8U                  ,  8-bit long
    EXTRN   _OSTaskSwHook

    EXTRN     _OSIntNestingCtr            ; Declared as 'CPU_INT08U'
    
    EXTRN     _OSTimeTick					; OSTimeTick()
    EXTRN     _OSIntExit                  ; OSIntExit()    
;    EXTRN   _OSISRStk


;********************************************************************************************************
;                                             OS_CTX_SAVE
;
; Description : This MACRO saves the CPU registers (i.e. CPU context) onto the current task's stack using 
;               the same order as they were saved in OSTaskStkInit().
;********************************************************************************************************

OS_CTX_SAVE:  MACRO
	flush es
	flush ls
	push all
    
    ENDM
    
;/*$PAGE*/
;********************************************************************************************************
;                                           OS_CTX_RESTORE
;
; Description : This MACRO restores the CPU registers (i.e. context) from the new task's stack in the 
;               reverse order of OS_CTX_SAVE (see above)
;********************************************************************************************************

OS_CTX_RESTORE:  MACRO
	pop all
	fetch es
	fetch ls
    ENDM
    
;/*$PAGE*/
;********************************************************************************************************
;                                            OS_ISR_ENTER
;
; Description : Interrupt service routine prologue for kernel-aware handler.
;               This macro implements the following code in assembly language:
;
;                  OS_ISR_ENTER
;                      OS_CTX_SAVE                   ; Call the macro: OS_CTX_SAVE
;                      OSIntNestingCtr++;
;                      if (OSIntNestingCtr == 1) {
;                          OSTCBCurPtr->StkPtr = SP;
;                      }
;
;               This MACRO is to be used by your assembly language based ISRs as follows:
;
;                  MyISR
;                      OS_ISR_ENTER
;                      ISR Body here
;                      OS_ISR_EXIT
;********************************************************************************************************

OS_ISR_ENTER:  MACRO
    OS_CTX_SAVE                                      ; Save processor registers on the stack
;$$$$                                                ; Implement: 'OSIntNestingCtr++;' 
;$$$$                                                ; Implement: 'if (OSIntNestingCtr == 1)'
;$$$$                                                ; Implement: '    OSTCBCurPtr->StkPtr = SP'

    ENDM
    
;/*$PAGE*/
;********************************************************************************************************
;                                             OS_ISR_EXIT
;
; Description : Interrupt service routine epilog for kernel-aware handler.
;               This macro implements the following code in assembly language:
;
;                  OS_ISR_EXIT:
;                      OSIntExit();					 ; Call the C function: OSIntExit();
;                      OS_CTX_RESTORE       		 ; Call the macro: OS_CTX_RESTORE
;					   Return from interrupt         ; CPU instruction to return from interrupt/exception
;********************************************************************************************************

OS_ISR_EXIT:  MACRO
;$$$$                                                ; Call 'OSIntExit()'
    OS_CTX_RESTORE                                   ; Restore processor registers from stack
;$$$$                                                ; CPU instruction to return from Interrupt/exception
    							                     
    ENDM

;********************************************************************************************************
;                                      CODE GENERATION DIRECTIVES
;********************************************************************************************************

;$$$$    section .text:CODE:ROOT
	SEGMENT CODETEXT,CODE 
    
;/*$PAGE*/
;*********************************************************************************************************
;                                         START MULTITASKING
;
; Description : This function is called by OSStart() to start the highest priority task that was created
;               by your application before calling OSStart().
;
; Arguments   : none
;
; Note(s)     : 1) The stack frame is assumed to look as follows:
;   
;                  OSTCBHighRdy->OSTCBStkPtr +  0  ---->  
;
;               2) OSStartHighRdy() MUST:
;                      a) Call OSTaskSwHook() then,
;                      b) Switch to the highest priority task.
;*********************************************************************************************************

_OSStartHighRdy:
    callw   	_OSTaskSwHook               ; OSTaskSwHook();
    
    ldw.a  	    _OSTCBHighRdyPtr     ;SP	= OSTCBHighRdyPtr->StkPtr
    ld
    ld
    st	    	msp

	ld 8							;those four are only used for developer
	ld [msp+8]
	addi -4
	st
	
    OS_CTX_RESTORE

	reti

;/*$PAGE*/
;*********************************************************************************************************
;                                       TASK LEVEL CONTEXT SWITCH
;
; Description : This function is called when a task makes a higher priority task ready-to-run.
;               The pseudo code is:
;
;                   OS_CTX_SAVE
;                   OSTCBCurPtr->SP = SP;
;                   OSTaskSwHook();
;                   OSPrioCur       = OSPrioHighRdy;
;                   OSTCBCurPtr     = OSTCBHighRdyPtr;
;                   SP              = OSTCBCurPtr->StkPtr;
;                   OS_CTX_RESTORE
;                   Return from interrupt/exception
;*********************************************************************************************************

_OSCtxSw:
	ld.b 63
	reqfs
	ret
	

_CTXSW_SR:
    OS_CTX_SAVE     ; Call context save macro

	ldw.a       	_OSTCBCurPtr     	     ;OSTCBCurPtr
    ld											;OSTCBCurPtr->StkPtr
	ld	    	    MSP
	st                   					;OSTCBCurPtr->StkPtr = SP;

    callw   	_OSTaskSwHook               ; OSTaskSwHook();
    	
	ldw.a   	_OSPrioCur	     ;OSPrioCur = OSPrioHighRdy
    ldw.a   	_OSPrioHighRdy
    ld.b      
    st.b  

    ldw.a   	_OSTCBCurPtr	     ;OSTCBCurPtr = OSTCBHighRdyPtr
    ldw.a   	_OSTCBHighRdyPtr
    ld      
    st 
    
    ldw.a  	    _OSTCBHighRdyPtr     ;SP = OSTCBCur->OSTCBStkPtr
    ld
    ld
    st	    	msp

	ld 8							;those four are only used for developer
	ld [msp+8]
	addi -4
	st

    OS_CTX_RESTORE	; Call context restore macro

    reti            ; Return from interrupt/exception 

;/*$PAGE*/
;*********************************************************************************************************
;                               PERFORM A CONTEXT SWITCH (From an ISR)
;
; Description : This function is called when an ISR makes a higher priority task ready-to-run.
;*********************************************************************************************************

_OSIntCtxSw:
	callw   	_OSTaskSwHook               ; OSTaskSwHook();
    
	ldw.a   	_OSPrioCur	     ;OSPrioCur = OSPrioHighRdy
    ldw.a   	_OSPrioHighRdy
    ld.b      
    st.b  

    ldw.a   	_OSTCBCurPtr	     ;OSTCBCurPtr = OSTCBHighRdyPtr
    ldw.a   	_OSTCBHighRdyPtr
    ld      
    st 

    ldw.a  	    _OSTCBHighRdyPtr     ;SP = OSTCBCur->OSTCBStkPtr
    ld
    ld
    st	    	msp

	ld 8							;those four are only used for developer
	ld [msp+8]
	addi -4
	st
	
    OS_CTX_RESTORE	; Call context restore macro

    reti            ; Return from interrupt/exception

;********************************************************************************************************
;                                              TICK ISR
;
; Description: This ISR handles tick interrupts.  This ISR uses the timer as the tick source.
;
; Notes      : 1) The following C pseudo-code describes the operations being performed in the code below.
;
;                 Save all the CPU registers
;                 if (OSIntNestingCtr == 0) {
;                     OSTCBCurPtr->StkPtr  = SP;
;                     SP                    = OSISRStk;     /* Use the ISR stack from now on           */
;                 }
;                 OSIntNestingCtr++;
;                 Enable interrupt nesting;                 /* Allow nesting of interrupts (if needed) */
;                 Clear the interrupt source;
;                 OSTimeTick();                             /* Call uC/OS-III's tick handler           */
;                 DISABLE general interrupts;               /* Must DI before calling OSIntExit()      */
;                 OSIntExit();
;                 if (OSIntNestingCtr == 0) {
;                     SP = OSTCBHighRdyPtr->StkPtr;         /* Restore the current task's stack        */
;                 }
;                 Restore the CPU registers
;                 Return from interrupt.
;
;              2) ALL ISRs should be written like this!
;
;              3) You MUST disable general interrupts BEFORE you call OSIntExit() because an interrupt
;                 COULD occur just as OSIntExit() returns and thus, the new ISR would save the SP of
;                 the ISR stack and NOT the SP of the task stack.  This of course will most likely cause
;                 the code to crash.  By disabling interrupts BEFORE OSIntExit(), interrupts would be
;                 disabled when OSIntExit() would return.
;
;              4) If you DON'T use a separate ISR stack then you don't need to disable general interrupts
;                 just before calling OSIntExit().  The pseudo-code for an ISR would thus look like this:
;
;                 Save all the CPU registers
;                 if (OSIntNestingCtr == 0) {
;                     OSTCBCurPtr->StkPtr = SP;
;                 }
;                 OSIntNestingCtr++;
;                 Enable interrupt nesting;                 /* Allow nesting of interrupts (if needed) */
;                 Clear the interrupt source;
;                 OSTimeTick();                             /* Call uC/OS-III's tick handler            */
;                 OSIntExit();
;                 Restore the CPU registers
;                 Return from interrupt.
;********************************************************************************************************
_OSTickISR:  	 
    
    OS_CTX_SAVE     ;
    
    dis             ; disable interrupts
    
    ldw.a			_OSIntNestingCtr	     ;if (OSIntNestingCtr == 0) 
    ld.b		             
    drop
    br.nz           TickISR1       

    ldw.a       	_OSTCBCurPtr     	     ;OSTCBCurPtr
    ld											;OSTCBCurPtr->StkPtr
	ld	    	    MSP
	st                   					;OSTCBCurPtr->StkPtr = SP;

TickISR1:
   	ldw.a			_OSIntNestingCtr        ;OSIntNestingCtr++  
	dup
	ld.b
	addi	1
	st.b
    	    	      
	enb       			    

	ld.i 			0xB8FD		;clear tick interrupt for timer 1
	tmr.write

	ld.b 10
	ld 2
	or.port						;enable timer interrupt
    	  
    callw	_OSTimeTick	  		;OSTimeTick();  
    
	callw	_OSIntExit          ;OSIntExit();     
	                                      
	OS_CTX_RESTORE
	    
	reti


    END
