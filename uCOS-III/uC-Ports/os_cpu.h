/*
*********************************************************************************************************
*                                               uC/OS-III
*                                        The Real-Time Kernel
*
*                          (c) Copyright 2008-2015, Micrium, Inc., Weston, FL
*                                          All Rights Reserved
*
*                                         $$$$ TEMPLATE
*                                         $$$$ Insert CPU Name
*                                         $$$$ Insert Compiler Name
*
* Filename     : os_cpu.h
* Version      : $$$$ V3.05.01
* By           : $$$$ JJL
*********************************************************************************************************
* Note(s)       : (1) This file is used to create a uC/OS-III port.  You can use this template as a
*                     starting point instead of typing everything from scratch.
*
*                     You should replace anything that is preceded by '$$$$' with target specific code.
*********************************************************************************************************
*/

#ifndef _OS_CPU_H
#define _OS_CPU_H

#ifdef  OS_CPU_GLOBALS
#define OS_CPU_EXT
#else
#define OS_CPU_EXT  extern
#endif

#ifdef __cplusplus
extern  "C" {
#endif


#include <system.h> 
#include <io_macro.h>


/*
*********************************************************************************************************
*                                             VARIABLES
*********************************************************************************************************
*/

typedef int int32_t;
typedef short int int16_t;
typedef char int8_t;

//typedef const int32_t int;  /*!< Read Only */
//typedef const int16_t short int;  /*!< Read Only */
//typedef const int8_t char;   /*!< Read Only */

//typedef __IO int32_t  int;
//typedef __IO int16_t  short int;
//typedef __IO int8_t   char;

//typedef __I int32_t int;  /*!< Read Only */
//typedef __I int16_t short int;  /*!< Read Only */
//typedef __I int8_t char;   /*!< Read Only */

typedef unsigned int uint32_t;
typedef unsigned short int uint16_t;
typedef unsigned char uint8_t;

//typedef const uint32_t unsigned int;  /*!< Read Only */
//typedef const uint16_t unsigned short int;  /*!< Read Only */
//typedef const uint8_t unsigned char;   /*!< Read Only */

//typedef __IO uint32_t  unsigned int;
//typedef __IO uint16_t unsigned short int;
//typedef __IO uint8_t  unsigned char;

//typedef __I uint32_t unsigned int;  /*!< Read Only */
//typedef __I uint16_t unsigned short int;  /*!< Read Only */
//typedef __I uint8_t unsigned char;   /*!< Read Only */

/*
typedef enum {RESET = 0, SET = !RESET} FlagStatus, ITStatus;

typedef enum {DISABLE = 0, ENABLE = !DISABLE} FunctionalState;
#define IS_FUNCTIONAL_STATE(STATE) (((STATE) == DISABLE) || ((STATE) == ENABLE))

typedef enum {ERROR = 0, SUCCESS = !ERROR} ErrorStatus;
*/


/*
*********************************************************************************************************
*                                             VARIABLES
*********************************************************************************************************
*/

//OS_CPU_EXT  CPU_STK  *OSISRStk;


/*
*********************************************************************************************************
*                                               MACROS
*
* Note(s): OS_TASK_SW()  invokes the task level context switch.
*
*          (1) On some processors, this corresponds to a call to OSCtxSw() which is an assemply language
*              function that performs the context switch.
*
*          (2) On some processors, you need to simulate an interrupt using a 'sowfate interrupt' or a
*              TRAP instruction.  Some compilers allow you to add in-line assembly language as shown.
*********************************************************************************************************
*/

#define  OS_TASK_SW()    OSCtxSw()               /* Task level context switch routine             */

// The structure of the registers saved/loaded by microprogram.
struct RegisterState {
	unsigned int ERAR;
	unsigned int BMP;
	unsigned int *ESP;
	unsigned int *LSP;
	void (*RAR)(void*);
	unsigned int EAR;
	unsigned int FMP;
	unsigned int LMP;
	unsigned int *ESB;
	unsigned int *LSB;
	unsigned int *MSB;
	unsigned int *MSL;
};
// RAR contains the address of the entry-point of the thread. So, the thread is to be started with a "function" which uses "ret" at the end to start the new thread. But this is generally true, address where to continue the execution of a thread is stored in RAR.
#define STKSizeUser 0x100
#define STKSizeEval 0x100
#define STKSizeLocal 0x100
#define STACK_ALIGNMENT 0x100
#define STACK_MASK 0xAA

/*
*********************************************************************************************************
*                                       TIMESTAMP CONFIGURATION
*
* Note(s) : (1) OS_TS_GET() is generally defined as CPU_TS_Get32() to allow CPU timestamp timer to be of
*               any data type size.
*
*           (2) For architectures that provide 32-bit or higher precision free running counters 
*               (i.e. cycle count registers):
*
*               (a) OS_TS_GET() may be defined as CPU_TS_TmrRd() to improve performance when retrieving
*                   the timestamp.  You would use CPU_TS_TmrRd() if this function returned the value of 
*                   a 32-bit free running timer 0x00000000 to 0xFFFFFFFF then roll over to 0x00000000.
*
*               (b) CPU_TS_TmrRd() MUST be configured to be greater or equal to 32-bits to avoid
*                   truncation of TS.
*
*               (c) The Timer must be an up counter.
*********************************************************************************************************
*/

#if      OS_CFG_TS_EN == 1u
#define  OS_TS_GET()               (CPU_TS)CPU_TS_Get32()   /* See Note #2a.                                          */
#else
#define  OS_TS_GET()               (CPU_TS)0u
#endif


/*
*********************************************************************************************************
*                                         FUNCTION PROTOTYPES
*********************************************************************************************************
*/

void  OSCtxSw        (void);
void  OSIntCtxSw     (void);
void  OSStartHighRdy (void);
extern void OSTickISR(void);
extern void CTXSW_SR(void);




#ifdef __cplusplus
}
#endif

#endif
